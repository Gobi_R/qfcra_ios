//
//  MenuDisplayControllerViewController.swift
 
//
//  Created by Karthikeyan A. on 25/01/18.
//  Copyright © 2018 DCI. All rights reserved.
//

import UIKit
 

class MenuDisplayControllerViewController: UIViewController,SlideMenuDelegate {
    let window = UIApplication.shared.keyWindow
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func openSideMenu(){
        let btn = UIButton(type: UIButton.ButtonType.custom)
        self.openVCSlideMenuButtonPressed(btn)
    }
    
    func openVCSlideMenuButtonPressed(_ sender : UIButton){
        guard let getTopVC = UIApplication.getTopMostViewController() else { return }
        if !getTopVC.className.isEmpty && getTopVC.className == "MenuViewController" {
           // print(getTopVC.className)
             return
        }
    
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                if let getTag = self.window?.viewWithTag(999) {
                    getTag.removeFromSuperview()
                }
                viewMenuBack.removeFromSuperview()
                
                
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        
         let menuVC: MenuViewController = UIStoryboard(storyboard: .main).instantiateViewController()
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.backgroundColor = UIColor.clear
        menuVC.view.tag = 999
        window?.addSubview(menuVC.view)
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion: { (finished: Bool) in
            
            menuVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        })
    }

    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            // DashBoard
            self.openViewControllerBasedOnIdentifier("DashBoardViewController")
          
           // self.openViewControllerBasedOnIdentifier("HomeViewController")
            break
        case 1:
            // my profile
            self.openViewControllerBasedOnIdentifier("MyProfileViewController")
            break
      
        case 2:
         // LR
             self.openViewControllerBasedOnIdentifier("LeaveRequestViewController")
            break
        case 3:
           //PR
           self.openViewControllerBasedOnIdentifier("ClaimsViewController")
            break
        case 4:
            //Claims
             self.openViewControllerBasedOnIdentifier("PRViewController")
            break
        case 5:
             // Budget
            self.openViewControllerBasedOnIdentifier("BudgetAllocationViewController")
            break
        case 6:
           //payroll
           self.openViewControllerBasedOnIdentifier("PayrollViewController")
            break
     case 7:
             // Logout
           self.openLogout()
        
            break
        
            
        default:
            print("default\n", terminator: "")
        }
        
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        
        let destViewController  = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
    //    print("destViewController \(destViewController.restorationIdentifier!)")
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        // print("topViewController \(topViewController.restorationIdentifier!)")
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
         
//             if(destViewController.isKind(of:TransferToViewController.self)) {
//                let transferVc = destViewController as! TransferToViewController
//                 transferVc.viewIdendity = "sideMenu"
//
//            }
            AppUtils.appUtilityPush(self, destViewController)
        }
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openLogout(){
        let logoutAlert = UIAlertController(title: "Logout", message: "Are you sure want to Logout?"
, preferredStyle: .alert)
        
        logoutAlert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action: UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        logoutAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        
        present(logoutAlert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
