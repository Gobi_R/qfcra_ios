
//

import UIKit

class CustomAlert: UIView, Modal, UITextViewDelegate {
    var backgroundView = UIView()
    var dialogView = UIView()
    var placeholderLabel : UILabel!
    var viewWasMoved: Bool = false
    var dialogViewHeight: CGFloat!
    
    convenience init(title:String) {
        self.init(frame: UIScreen.main.bounds)
        initialize(title: title)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize(title:String){
        registerForKeyboardNotifications()
        dialogView.clipsToBounds = true
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
        addSubview(backgroundView)
        
        let dialogViewWidth = frame.width-64
        
        let titleLabel = UILabel(frame: CGRect(x: 8, y: 8, width: dialogViewWidth-16, height: 30))
        titleLabel.text = "Comment:"
        titleLabel.font =  UIFont.regularFontOfSize(size: 13)
        titleLabel.textAlignment = .left
        dialogView.addSubview(titleLabel)
        
        let separatorLineView = UIView()
        separatorLineView.frame.origin = CGPoint(x: 0, y: titleLabel.frame.height + 8)
        separatorLineView.frame.size = CGSize(width: dialogViewWidth, height: 1)
        separatorLineView.backgroundColor = UIColor.groupTableViewBackground
        dialogView.addSubview(separatorLineView)
        
        let textView = UITextView()
        textView.frame.origin =  CGPoint(x: 8, y: separatorLineView.frame.height + separatorLineView.frame.origin.y + 8)
        textView.frame.size = CGSize(width: dialogViewWidth - 32 , height: dialogViewWidth - 100)
        textView.textAlignment = NSTextAlignment.justified
        textView.textColor = UIColor.black
        textView.backgroundColor = .clear
        textView.layer.cornerRadius = 4
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.font =  UIFont.regularFontOfSize(size: 13)
        //textView.text = "Type your comment here"
        dialogView.addSubview(textView)
        textView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Type your comment here"
        placeholderLabel.font = UIFont.regularFontOfSize(size: 13)  //italicSystemFont(ofSize: (textView.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
        
        let button: UIButton = UIButton(frame: CGRect(x: 10, y: textView.frame.height + textView.frame.origin.y + 8, width: dialogViewWidth/2 - 20, height: 42))
        button.backgroundColor = .appBGColor
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.tag = 11
        button.layer.cornerRadius = 5
        button.titleLabel!.font =  UIFont.boldSystemFontOfSize(size: 15)
        button.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
        dialogView.addSubview(button)
        
        let CancelButton:UIButton = UIButton(frame: CGRect(x: dialogViewWidth/2 + 10, y: textView.frame.height + textView.frame.origin.y + 8, width: dialogViewWidth/2 - 20, height: 42))
        CancelButton.backgroundColor = .appBGColor
        CancelButton.setTitle("Cancel", for: .normal)
        CancelButton.setTitleColor(.white, for: .normal)
        CancelButton.tag = 11
        CancelButton.layer.cornerRadius = 5
        CancelButton.titleLabel!.font =  UIFont.boldSystemFontOfSize(size: 15)
        CancelButton.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
        dialogView.addSubview(CancelButton)
        
        dialogViewHeight = titleLabel.frame.height + 8 + separatorLineView.frame.height + 8 + textView.frame.height + 8 + CancelButton.frame.height + 8
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 6
        addSubview(dialogView)
//        let imageView = UIImageView()
//        imageView.frame.origin = CGPoint(x: 8, y: separatorLineView.frame.height + separatorLineView.frame.origin.y + 8)
//        imageView.frame.size = CGSize(width: dialogViewWidth - 16 , height: dialogViewWidth - 16)
//        imageView.image = image
//        imageView.layer.cornerRadius = 4
//        imageView.clipsToBounds = true
//        dialogView.addSubview(imageView)
//
//        let dialogViewHeight = titleLabel.frame.height + 8 + separatorLineView.frame.height + 8 + imageView.frame.height + 8
//
//        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
//        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
//        dialogView.backgroundColor = UIColor.white
//        dialogView.layer.cornerRadius = 6
//        addSubview(dialogView)
    }
    
    @objc func didTappedOnBackgroundView(){
        self.endEditing(true)
        //dismiss(animated: true)
    }
    
    @objc private func buttonClicked(_ sender: UIButton) {
        if sender.tag == 10 { // approve
             didTappedOnBackgroundView()
            
        }else if sender.tag == 11{
            //cancel
            dismiss(animated: true)
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            endEditing(true)
            return false
        }
        return true
    }
    
    @objc func  keyboardWillShow(_ notification: Notification) {
         let info = notification.userInfo!
        guard let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else {return}
        if dialogView.frame.origin.y != 0 {
            self.dialogView.frame.origin.y -= keyboardSize.height - 80
        }
    }
    
    @objc func keyboardWillHidden(_ notification: Notification) {
   //     let info = notification.userInfo!
    //    guard let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else {return}
        var getDialog = dialogView.frame
        getDialog.origin.y = CGFloat(UserDefaults.standard.float(forKey: "dialogFrame"))
        dialogView.frame = getDialog
        
       //   UserDefaults.standard.set(alert.dialogView.frame.origin.y, forKey: "dialogFrame")
//        if dialogView.frame.origin.y != dialogViewHeight {
//            self.dialogView.frame.origin.y = dialogViewHeight
//        }
    }
    
    func registerForKeyboardNotifications() {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
