 
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController {
    
    
    /**
     *  Array to display menu options
     */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
     *  Transparent button to hide menu
     */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
     *  Array containing menu options
     */
    // var arrayMenuOptions = [Dictionary<String,String>]()
    
    
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    
    let numberOfMenu = 8
    let menuCellIdentifier = "cellMenu"
    
    enum MenuIndex: Int {
        case DASHBOARD,PERSONALPROFILE,LEAVEREQUESTS,EXPENSECLAIMS,PURCHASEREQUISITION,BUDGETREALLOCATION,PAYROLL,LOGOUT
        static let menuNames = [
            DASHBOARD : QFCRA_SideMenu.QFCRA_Dash, PERSONALPROFILE : QFCRA_SideMenu.QFCRA_PP,  LEAVEREQUESTS : QFCRA_SideMenu.QFCRA_LR,  EXPENSECLAIMS : QFCRA_SideMenu.QFCRA_EC, PURCHASEREQUISITION : QFCRA_SideMenu.QFCRA_PR, BUDGETREALLOCATION : QFCRA_SideMenu.QFCRA_Bra, PAYROLL : QFCRA_SideMenu.QFCRA_Payroll, LOGOUT : QFCRA_SideMenu.QFCRA_Logout]
        
        func menuName() -> String {
            guard let menuName = MenuIndex.menuNames[self] else { return "" }
            print (menuName)
            return menuName
        }
        
        func menuImage() -> UIImage {
            print(menuName())
            return UIImage(named: "\(menuName())")!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        tblMenuOptions.separatorInset = .zero
        tblMenuOptions.layoutMargins = .zero

    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        //  self.tblMenuOptions.backgroundColor = UIColor(patternImage: UIImage(named: "blue-login")!)
      
        self.tblMenuOptions.reloadData()
        self.tblMenuOptions.animate(animation:  TableViewAnimation.Cell.fade(duration: 0.8))
    
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        self.view.backgroundColor = UIColor.clear
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
   
    
    
}
extension MenuViewController : UITableViewDataSource, UITableViewDelegate{
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: menuCellIdentifier)!
        
        //  cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.preservesSuperviewLayoutMargins = false
        //  cell.separatorInset = UIEdgeInsets.zero
        // cell.layoutMargins = UIEdgeInsets.zero
        cell.backgroundColor = .clear
        
       
      //  let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        if let menuIndex = MenuIndex(rawValue: indexPath.row) {
            cell.textLabel?.text = menuIndex.menuName()
            cell.imageView?.image = menuIndex.menuImage()
             cell.imageView?.contentMode = .scaleAspectFit
        }
//        let screenSize = tableView.bounds
//        let separatorHeight = CGFloat(2.0) //
//        let imageView = UIImageView(image: UIImage(named: "bottomBorder")!)
//        imageView.frame = CGRect(x: -20, y: cell.frame.size.height-separatorHeight, width: screenSize.width  , height: 4)
//        cell.contentView.addSubview(imageView)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let menuIndex = MenuIndex(rawValue: indexPath.row) {
//          let menuname = menuIndex.menuName()
//            if menuname ==QFCRA_SideMenu.XTRM_Exchange || menuname ==QFCRA_SideMenu.XTRM_Fund || menuname ==QFCRA_SideMenu.XTRM_Transfer || menuname ==QFCRA_SideMenu.XTRM_Wallets
//
//            {
//                print(menuname)
//                if getWalletStatus() == true{
//                    // navigate to wallet page
//                   return
//                }
//
//            }
        }
        
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfMenu
    }
}
