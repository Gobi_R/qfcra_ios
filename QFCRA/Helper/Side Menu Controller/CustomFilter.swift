//
//  CustomFilter.swift
//  QFCRA
//
//  Created by Aravind on 03/12/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit
typealias v2CB = (_ infoToReturn :String) ->()
class CustomFilter: UIView, Modal, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var completionBlock:v2CB?
    var backgroundView = UIView()
    var dialogView = UIView ()
    var footerLineView = UIView()
    var acceptBtn = UIButton()
    var cancelBtn = UIButton()
    
    var viewDate = UIView()
    var viewStaff = UIView()
    var viewApproval = UIView()
    
    var periodButton = UIButton()
    var periodView = UIView()
    var periodTxtFld = UITextField()
    
    var dateButton = UIButton()
    var staffButton = UIButton()
    var approvalButton = UIButton()
    
    var dialogViewWidth = CGFloat()
    var viewTitle = ""
    var viewHeaderLine = UILabel()
    
    //Mark : - View Date Properties
    let datePicker = UIDatePicker()
    var labelStartDate = UILabel()
    var labelEndDate = UILabel()
    var imgvwStartDate = UIImageView()
    var imgvwEndDate = UIImageView()
    var lblStartDateTxt = UILabel()
    var lblEndDateTxt = UILabel()
    var txtFldStartDate = UITextField()
    var txtFldEndDate = UITextField()
    
    //Mark : - Staff Name Properties
    
    let arrayStaff = ["Caroline", "Dany"]
    var staffTableView : UITableView!
    
    //Mark : - Approval Status
    let arrayStatusList = ["Approved", "Rejected", "Hold"]
    let arrayStatus = NSMutableArray()
    var statusTableView : UITableView!

    convenience init(title:String) {
        self.init(frame: UIScreen.main.bounds)
        initialize(title: title)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize(title:String){
        
        viewTitle = title
        
        if(viewTitle == "Purchase Request") {
            dialogView.clipsToBounds = true
            
            backgroundView.frame = frame
            backgroundView.backgroundColor = UIColor.black
            backgroundView.alpha = 0.6
            backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
            addSubview(backgroundView)
            
            dialogViewWidth = frame.width-64
            
            let labelFilter:UILabel = UILabel(frame: CGRect(x: 0, y: 10, width: dialogViewWidth, height: 30))
            labelFilter.text = "Filter List"
            labelFilter.textAlignment = .center
            labelFilter.font = UIFont.boldSystemFontOfSize(size: 15)
            dialogView.addSubview(labelFilter)
            
           viewHeaderLine.frame = CGRect(x: 10, y: labelFilter.frame.origin.y + labelFilter.frame.height+2, width: dialogViewWidth-20, height: 1)
            viewHeaderLine.backgroundColor = UIColor.lightGray
            dialogView.addSubview(viewHeaderLine)
            
            
            
            dateButton.frame = CGRect(x: 10, y: viewHeaderLine.frame.origin.y + 8, width: dialogViewWidth * 0.4, height: 30)
            dateButton.setTitle("Date:", for: .normal)
            dateButton.titleLabel?.textAlignment = .left
            dateButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            dateButton.contentHorizontalAlignment = .left
            dateButton.setTitleColor(UIColor.black, for:.normal)
            //dateButton.addTarget(self, action: #sel, for: <#T##UIControl.Event#>)
            dateButton.addTarget(self, action: #selector(didDateTapped), for: .touchUpInside)
            //dialogView.addSubview(dateButton)
            
            datePicker.datePickerMode = .date
            
            datePicker.addTarget(self, action: #selector(datePickervalueChanged(currentPicker:)), for: .valueChanged)
            
            viewDate.frame = CGRect(x: dialogViewWidth/2 - 10, y: dateButton.frame.origin.y, width: dialogViewWidth/2, height: 170)
            viewDate.backgroundColor = UIColor.clear
            viewDate.layer.cornerRadius = 5.0
            viewDate.layer.borderWidth = 1.0
            viewDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewDate.isHidden = true
            
            labelStartDate.frame = CGRect(x: 10, y: 10, width: viewDate.frame.width - 20, height: 30)
            labelStartDate.text = "Start Date"
            labelStartDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelStartDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelStartDate)
            
            imgvwStartDate.frame = CGRect(x: 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwStartDate.backgroundColor = UIColor.white
            imgvwStartDate.contentMode = .scaleAspectFill
            imgvwStartDate.image = UIImage(named: "date_icon")
            imgvwStartDate.clipsToBounds = true
            viewDate.addSubview(imgvwStartDate)
            
            lblStartDateTxt.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 30), height: 30)
            lblStartDateTxt.text = "12 Nov 2018"
            lblStartDateTxt.textColor = UIColor.black
            
            lblStartDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblStartDateTxt)
            
            txtFldStartDate.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 5, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 20), height: 30)
            txtFldStartDate.layer.cornerRadius = 5.0
            txtFldStartDate.layer.borderWidth = 1.0
            txtFldStartDate.tag = 10
            txtFldStartDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldStartDate.inputView = datePicker
            txtFldStartDate.inputAccessoryView = toolBar()
            txtFldStartDate.delegate = self
            //txtFldStartDate.forEach{$0.inputView = datePicker;$0.inputAccessoryView = toolBar()}
            viewDate.addSubview(txtFldStartDate)
            
            
            labelEndDate.frame = CGRect(x: 10, y: txtFldStartDate.frame.origin.y + txtFldStartDate.frame.height + 10, width: viewDate.frame.width - 20, height: 30)
            labelEndDate.text = "End Date"
            labelEndDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelEndDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelEndDate)
            
            imgvwEndDate.frame = CGRect(x: 10, y: labelEndDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwEndDate.backgroundColor = UIColor.white
            imgvwEndDate.contentMode = .scaleAspectFill
            imgvwEndDate.image = UIImage(named: "date_icon")
            imgvwEndDate.clipsToBounds = true
            viewDate.addSubview(imgvwEndDate)
            
            lblEndDateTxt.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 10, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 30), height: 30)
            lblEndDateTxt.text = "12 Nov 2018"
            lblEndDateTxt.textColor = UIColor.black
            lblEndDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblEndDateTxt)
            
            txtFldEndDate.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 5, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 20), height: 30)
            txtFldEndDate.layer.cornerRadius = 5.0
            txtFldEndDate.layer.borderWidth = 1.0
            txtFldEndDate.tag = 11
            txtFldEndDate.inputView = datePicker
            txtFldEndDate.inputAccessoryView = toolBar()
            txtFldEndDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldEndDate.delegate = self
            viewDate.addSubview(txtFldEndDate)
            
            
           // dialogView.addSubview(viewDate)
            
            
            
            staffButton.frame = CGRect(x: 10, y: viewHeaderLine.frame.origin.y + 8, width: dialogViewWidth * 0.4, height: 30)
            staffButton.setTitle("Requestor Name:", for: .normal)
            staffButton.contentHorizontalAlignment = .left
            staffButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            staffButton.setTitleColor(UIColor.black, for:.normal)
            staffButton.addTarget(self, action: #selector(didStaffTapped), for: .touchUpInside)
            dialogView.addSubview(staffButton)
            
            viewStaff.frame = CGRect(x: dialogViewWidth/2 - 10, y: staffButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewStaff.backgroundColor = UIColor.clear
            viewStaff.layer.cornerRadius = 5.0
            viewStaff.layer.borderWidth = 1.0
            viewStaff.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewStaff.isHidden = true
            
            staffTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewStaff.frame.size.width - 10, height: viewStaff.frame.height - 20))
            staffTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
            staffTableView.dataSource = self
            staffTableView.delegate = self
            viewStaff.addSubview(staffTableView)
            
            dialogView.addSubview(viewStaff)
            
            approvalButton.frame = CGRect(x: 10, y: staffButton.frame.origin.y + 38, width: dialogViewWidth * 0.4, height: 30)
            approvalButton.setTitle("Approval Status:", for: .normal)
            approvalButton.titleLabel?.textAlignment = .left
            approvalButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            approvalButton.contentHorizontalAlignment = .left
            approvalButton.setTitleColor(UIColor.black, for:.normal)
            approvalButton.addTarget(self, action: #selector(didApprovalTapped), for: .touchUpInside)
            dialogView.addSubview(approvalButton)
            
            viewApproval.frame = CGRect(x: dialogViewWidth/2 - 10, y: approvalButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewApproval.backgroundColor = UIColor.clear
            viewApproval.layer.cornerRadius = 5.0
            viewApproval.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewApproval.layer.borderWidth = 1.0
            viewApproval.isHidden = true
            
            
            statusTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewApproval.frame.size.width - 10, height: viewApproval.frame.height - 20))
            statusTableView.register(UITableViewCell.self, forCellReuseIdentifier: "StatusCell")
            statusTableView.dataSource = self
            statusTableView.delegate = self
            statusTableView.separatorStyle = .none
            viewApproval.addSubview(statusTableView)
            
            dialogView.addSubview(viewApproval)
            
            footerLineView.frame = CGRect(x: 10, y: approvalButton.frame.origin.y + 38, width: dialogViewWidth-20, height: 1)
            footerLineView.backgroundColor = UIColor.lightGray
            dialogView.addSubview(footerLineView)
            
            acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            acceptBtn.backgroundColor = UIColor.applyButtonColor
            acceptBtn.layer.cornerRadius = acceptBtn.frame.height / 2
            acceptBtn.setTitle("Apply", for: UIControl.State.normal)
            acceptBtn.addTarget(self, action: #selector(acceptBtnTapped), for: .touchUpInside)
            acceptBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            acceptBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            
            dialogView.addSubview(acceptBtn)
            
            cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2
            cancelBtn.backgroundColor = UIColor.appBGColor
            cancelBtn.setTitle("Cancel", for: UIControl.State.normal)
            cancelBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            cancelBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            cancelBtn.addTarget(self, action: #selector(cancelBtnTapped), for: .touchUpInside)
            dialogView.addSubview(cancelBtn)
            
            
            
            
            let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
            dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
            dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
            dialogView.backgroundColor = UIColor.white
            dialogView.layer.cornerRadius = 6
            addSubview(dialogView)
        }
        else if(viewTitle == "Expense Claims" || viewTitle == "Budget") {
            dialogView.clipsToBounds = true
            
            backgroundView.frame = frame
            backgroundView.backgroundColor = UIColor.black
            backgroundView.alpha = 0.6
            backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
            addSubview(backgroundView)
            
            dialogViewWidth = frame.width-64
            
            let labelFilter:UILabel = UILabel(frame: CGRect(x: 0, y: 10, width: dialogViewWidth, height: 30))
            labelFilter.text = "Filter List"
            labelFilter.textAlignment = .center
            labelFilter.font = UIFont.boldSystemFontOfSize(size: 15)
            dialogView.addSubview(labelFilter)
            
            let viewHeaderLine:UILabel = UILabel(frame: CGRect(x: 10, y: labelFilter.frame.origin.y + labelFilter.frame.height+2, width: dialogViewWidth-20, height: 1))
            viewHeaderLine.backgroundColor = UIColor.lightGray
            dialogView.addSubview(viewHeaderLine)
            
            dateButton.frame = CGRect(x: 10, y: viewHeaderLine.frame.origin.y + 8, width: dialogViewWidth * 0.4, height: 30)
            dateButton.setTitle("Date:", for: .normal)
            dateButton.titleLabel?.textAlignment = .left
            dateButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            dateButton.contentHorizontalAlignment = .left
            dateButton.setTitleColor(UIColor.black, for:.normal)
            //dateButton.addTarget(self, action: #sel, for: <#T##UIControl.Event#>)
            dateButton.addTarget(self, action: #selector(didDateTapped), for: .touchUpInside)
            dialogView.addSubview(dateButton)
            
            datePicker.datePickerMode = .date
            
            datePicker.addTarget(self, action: #selector(datePickervalueChanged(currentPicker:)), for: .valueChanged)
            
            viewDate.frame = CGRect(x: dialogViewWidth/2 - 10, y: dateButton.frame.origin.y, width: dialogViewWidth/2, height: 170)
            viewDate.backgroundColor = UIColor.clear
            viewDate.layer.cornerRadius = 5.0
            viewDate.layer.borderWidth = 1.0
            viewDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewDate.isHidden = true
            
            labelStartDate.frame = CGRect(x: 10, y: 10, width: viewDate.frame.width - 20, height: 30)
            labelStartDate.text = "Start Date"
            labelStartDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelStartDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelStartDate)
            
            imgvwStartDate.frame = CGRect(x: 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwStartDate.backgroundColor = UIColor.white
            imgvwStartDate.contentMode = .scaleAspectFill
            imgvwStartDate.image = UIImage(named: "date_icon")
            imgvwStartDate.clipsToBounds = true
            viewDate.addSubview(imgvwStartDate)
            
            lblStartDateTxt.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 30), height: 30)
            lblStartDateTxt.text = "12 Nov 2018"
            lblStartDateTxt.textColor = UIColor.black
            
            lblStartDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblStartDateTxt)
            
            txtFldStartDate.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 5, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 20), height: 30)
            txtFldStartDate.layer.cornerRadius = 5.0
            txtFldStartDate.layer.borderWidth = 1.0
            txtFldStartDate.tag = 10
            txtFldStartDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldStartDate.inputView = datePicker
            txtFldStartDate.inputAccessoryView = toolBar()
            txtFldStartDate.delegate = self
            //txtFldStartDate.forEach{$0.inputView = datePicker;$0.inputAccessoryView = toolBar()}
            viewDate.addSubview(txtFldStartDate)
            
            
            labelEndDate.frame = CGRect(x: 10, y: txtFldStartDate.frame.origin.y + txtFldStartDate.frame.height + 10, width: viewDate.frame.width - 20, height: 30)
            labelEndDate.text = "End Date"
            labelEndDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelEndDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelEndDate)
            
            imgvwEndDate.frame = CGRect(x: 10, y: labelEndDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwEndDate.backgroundColor = UIColor.white
            imgvwEndDate.contentMode = .scaleAspectFill
            imgvwEndDate.image = UIImage(named: "date_icon")
            imgvwEndDate.clipsToBounds = true
            viewDate.addSubview(imgvwEndDate)
            
            lblEndDateTxt.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 10, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 30), height: 30)
            lblEndDateTxt.text = "12 Nov 2018"
            lblEndDateTxt.textColor = UIColor.black
            lblEndDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblEndDateTxt)
            
            txtFldEndDate.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 5, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 20), height: 30)
            txtFldEndDate.layer.cornerRadius = 5.0
            txtFldEndDate.layer.borderWidth = 1.0
            txtFldEndDate.tag = 11
            txtFldEndDate.inputView = datePicker
            txtFldEndDate.inputAccessoryView = toolBar()
            txtFldEndDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldEndDate.delegate = self
            viewDate.addSubview(txtFldEndDate)
            
            
            dialogView.addSubview(viewDate)
            
            
            
            staffButton.frame = CGRect(x: 10, y: dateButton.frame.origin.y + 38, width: dialogViewWidth * 0.4, height: 30)
            staffButton.setTitle("Staff Name:", for: .normal)
            staffButton.contentHorizontalAlignment = .left
            staffButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            staffButton.setTitleColor(UIColor.black, for:.normal)
            staffButton.addTarget(self, action: #selector(didStaffTapped), for: .touchUpInside)
           // dialogView.addSubview(staffButton)
            
            viewStaff.frame = CGRect(x: dialogViewWidth/2 - 10, y: staffButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewStaff.backgroundColor = UIColor.clear
            viewStaff.layer.cornerRadius = 5.0
            viewStaff.layer.borderWidth = 1.0
            viewStaff.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewStaff.isHidden = true
            
            staffTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewStaff.frame.size.width - 10, height: viewStaff.frame.height - 20))
            staffTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
            staffTableView.dataSource = self
            staffTableView.delegate = self
            viewStaff.addSubview(staffTableView)
            
          //  dialogView.addSubview(viewStaff)
            
            approvalButton.frame = CGRect(x: 10, y: dateButton.frame.origin.y + 38, width: dialogViewWidth * 0.4, height: 30)
            approvalButton.setTitle("Approval Status:", for: .normal)
            approvalButton.titleLabel?.textAlignment = .left
            approvalButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            approvalButton.contentHorizontalAlignment = .left
            approvalButton.setTitleColor(UIColor.black, for:.normal)
            approvalButton.addTarget(self, action: #selector(didApprovalTapped), for: .touchUpInside)
            dialogView.addSubview(approvalButton)
            
            viewApproval.frame = CGRect(x: dialogViewWidth/2 - 10, y: approvalButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewApproval.backgroundColor = UIColor.clear
            viewApproval.layer.cornerRadius = 5.0
            viewApproval.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewApproval.layer.borderWidth = 1.0
            viewApproval.isHidden = true
            
            
            statusTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewApproval.frame.size.width - 10, height: viewApproval.frame.height - 20))
            statusTableView.register(UITableViewCell.self, forCellReuseIdentifier: "StatusCell")
            statusTableView.dataSource = self
            statusTableView.delegate = self
            statusTableView.separatorStyle = .none
            viewApproval.addSubview(statusTableView)
            
            dialogView.addSubview(viewApproval)
            
            footerLineView.frame = CGRect(x: 10, y: approvalButton.frame.origin.y + 38, width: dialogViewWidth-20, height: 1)
            footerLineView.backgroundColor = UIColor.lightGray
            dialogView.addSubview(footerLineView)
            
            acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            acceptBtn.backgroundColor = UIColor.applyButtonColor
            acceptBtn.layer.cornerRadius = acceptBtn.frame.height / 2
            acceptBtn.setTitle("Apply", for: UIControl.State.normal)
            acceptBtn.addTarget(self, action: #selector(acceptBtnTapped), for: .touchUpInside)
            acceptBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            acceptBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            
            dialogView.addSubview(acceptBtn)
            
            cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2
            cancelBtn.backgroundColor = UIColor.appBGColor
            cancelBtn.setTitle("Cancel", for: UIControl.State.normal)
            cancelBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            cancelBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            cancelBtn.addTarget(self, action: #selector(cancelBtnTapped), for: .touchUpInside)
            dialogView.addSubview(cancelBtn)
            
            
            
            
            let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
            dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
            dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
            dialogView.backgroundColor = UIColor.white
            dialogView.layer.cornerRadius = 6
            addSubview(dialogView)
        }
        else if(viewTitle == "Payroll")
        {
            dialogView.clipsToBounds = true
            
            backgroundView.frame = frame
            backgroundView.backgroundColor = UIColor.black
            backgroundView.alpha = 0.6
            backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
            addSubview(backgroundView)
            
            dialogViewWidth = frame.width-64
            
            let labelFilter:UILabel = UILabel(frame: CGRect(x: 0, y: 10, width: dialogViewWidth, height: 30))
            labelFilter.text = "Filter List"
            labelFilter.textAlignment = .center
            labelFilter.font = UIFont.boldSystemFontOfSize(size: 15)
            dialogView.addSubview(labelFilter)
            
            viewHeaderLine.frame = CGRect(x: 10, y: labelFilter.frame.origin.y + labelFilter.frame.height+2, width: dialogViewWidth-20, height: 1)
            viewHeaderLine.backgroundColor = UIColor.lightGray
            dialogView.addSubview(viewHeaderLine)
            
            //periodButton.frame = CGRect(x: 10, y: viewHeaderLine.frame.origin.y + 8, width: dialogViewWidth * 0.4, height: 30)
            periodButton.frame = CGRect(x: 10, y: viewHeaderLine.frame.origin.y + 8, width: dialogViewWidth * 0.4, height: 30)
            periodButton.setTitle("Period ID:", for: .normal)
            periodButton.titleLabel?.textAlignment = .left
            periodButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            periodButton.contentHorizontalAlignment = .left
            periodButton.setTitleColor(UIColor.black, for:.normal)
            //dateButton.addTarget(self, action: #sel, for: <#T##UIControl.Event#>)
            periodButton.addTarget(self, action: #selector(didPeriodBtnTapped), for: .touchUpInside)
            dialogView.addSubview(periodButton)
            
            periodView.frame = CGRect(x: dialogViewWidth/2 - 10, y: periodButton.frame.origin.y, width: dialogViewWidth/2, height: 70)
            periodView.backgroundColor = UIColor.clear
            periodView.layer.cornerRadius = 5.0
            periodView.layer.borderWidth = 1.0
            periodView.layer.borderColor = UIColor.filterBorderColor.cgColor
            periodView.isHidden = true
            
            periodTxtFld.frame = CGRect(x: 10, y: 10, width: (periodView.frame.width) - 20, height: 35)
            periodTxtFld.font = UIFont.regularFontOfSize(size: 14.0)
            periodTxtFld.textColor = UIColor.filterTxtColor
            periodTxtFld.delegate = self
            periodView.addSubview(periodTxtFld)
            
             let lineView:UIView = UIView(frame: CGRect(x: 10, y: periodTxtFld.frame.origin.y + periodTxtFld.frame.size.height + 1, width: (periodView.frame.width) - 20, height: 1))
            lineView.backgroundColor = UIColor.darkGray
            periodView.addSubview(lineView)
            
            dialogView.addSubview(periodView)
            
            dateButton.frame = CGRect(x: 10, y: periodButton.frame.origin.y + periodButton.frame.size.height + 8, width: dialogViewWidth * 0.4, height: 30)
            dateButton.setTitle("Date:", for: .normal)
            dateButton.titleLabel?.textAlignment = .left
            dateButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            dateButton.contentHorizontalAlignment = .left
            dateButton.setTitleColor(UIColor.black, for:.normal)
            //dateButton.addTarget(self, action: #sel, for: <#T##UIControl.Event#>)
            dateButton.addTarget(self, action: #selector(didDateTapped), for: .touchUpInside)
            dialogView.addSubview(dateButton)
            
            datePicker.datePickerMode = .date
            
            datePicker.addTarget(self, action: #selector(datePickervalueChanged(currentPicker:)), for: .valueChanged)
            
            viewDate.frame = CGRect(x: dialogViewWidth/2 - 10, y: dateButton.frame.origin.y, width: dialogViewWidth/2, height: 170)
            viewDate.backgroundColor = UIColor.clear
            viewDate.layer.cornerRadius = 5.0
            viewDate.layer.borderWidth = 1.0
            viewDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewDate.isHidden = true
            
            labelStartDate.frame = CGRect(x: 10, y: 10, width: viewDate.frame.width - 20, height: 30)
            labelStartDate.text = "Start Date"
            labelStartDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelStartDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelStartDate)
            
            imgvwStartDate.frame = CGRect(x: 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwStartDate.backgroundColor = UIColor.white
            imgvwStartDate.contentMode = .scaleAspectFill
            imgvwStartDate.image = UIImage(named: "date_icon")
            imgvwStartDate.clipsToBounds = true
            viewDate.addSubview(imgvwStartDate)
            
            lblStartDateTxt.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 30), height: 30)
            lblStartDateTxt.text = "12 Nov 2018"
            lblStartDateTxt.textColor = UIColor.black
            
            lblStartDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblStartDateTxt)
            
            txtFldStartDate.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 5, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 20), height: 30)
            txtFldStartDate.layer.cornerRadius = 5.0
            txtFldStartDate.layer.borderWidth = 1.0
            txtFldStartDate.tag = 10
            txtFldStartDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldStartDate.inputView = datePicker
            txtFldStartDate.inputAccessoryView = toolBar()
            txtFldStartDate.delegate = self
            //txtFldStartDate.forEach{$0.inputView = datePicker;$0.inputAccessoryView = toolBar()}
            viewDate.addSubview(txtFldStartDate)
            
            
            labelEndDate.frame = CGRect(x: 10, y: txtFldStartDate.frame.origin.y + txtFldStartDate.frame.height + 10, width: viewDate.frame.width - 20, height: 30)
            labelEndDate.text = "End Date"
            labelEndDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelEndDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelEndDate)
            
            imgvwEndDate.frame = CGRect(x: 10, y: labelEndDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwEndDate.backgroundColor = UIColor.white
            imgvwEndDate.contentMode = .scaleAspectFill
            imgvwEndDate.image = UIImage(named: "date_icon")
            imgvwEndDate.clipsToBounds = true
            viewDate.addSubview(imgvwEndDate)
            
            lblEndDateTxt.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 10, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 30), height: 30)
            lblEndDateTxt.text = "12 Nov 2018"
            lblEndDateTxt.textColor = UIColor.black
            lblEndDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblEndDateTxt)
            
            txtFldEndDate.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 5, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 20), height: 30)
            txtFldEndDate.layer.cornerRadius = 5.0
            txtFldEndDate.layer.borderWidth = 1.0
            txtFldEndDate.tag = 11
            txtFldEndDate.inputView = datePicker
            txtFldEndDate.inputAccessoryView = toolBar()
            txtFldEndDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldEndDate.delegate = self
            viewDate.addSubview(txtFldEndDate)
            
            
            dialogView.addSubview(viewDate)
            
            
            
            staffButton.frame = CGRect(x: 10, y: dateButton.frame.origin.y + 38, width: dialogViewWidth * 0.4, height: 30)
            staffButton.setTitle("Staff Name:", for: .normal)
            staffButton.contentHorizontalAlignment = .left
            staffButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            staffButton.setTitleColor(UIColor.black, for:.normal)
            staffButton.addTarget(self, action: #selector(didStaffTapped), for: .touchUpInside)
           // dialogView.addSubview(staffButton)
            
            viewStaff.frame = CGRect(x: dialogViewWidth/2 - 10, y: staffButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewStaff.backgroundColor = UIColor.clear
            viewStaff.layer.cornerRadius = 5.0
            viewStaff.layer.borderWidth = 1.0
            viewStaff.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewStaff.isHidden = true
            
            staffTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewStaff.frame.size.width - 10, height: viewStaff.frame.height - 20))
            staffTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
            staffTableView.dataSource = self
            staffTableView.delegate = self
          //  viewStaff.addSubview(staffTableView)
            
            dialogView.addSubview(viewStaff)
            
            approvalButton.frame = CGRect(x: 10, y: staffButton.frame.origin.y + 38, width: dialogViewWidth * 0.4, height: 30)
            approvalButton.setTitle("Approval Status:", for: .normal)
            approvalButton.titleLabel?.textAlignment = .left
            approvalButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            approvalButton.contentHorizontalAlignment = .left
            approvalButton.setTitleColor(UIColor.black, for:.normal)
            approvalButton.addTarget(self, action: #selector(didApprovalTapped), for: .touchUpInside)
          //  dialogView.addSubview(approvalButton)
            
            viewApproval.frame = CGRect(x: dialogViewWidth/2 - 10, y: approvalButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewApproval.backgroundColor = UIColor.clear
            viewApproval.layer.cornerRadius = 5.0
            viewApproval.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewApproval.layer.borderWidth = 1.0
            viewApproval.isHidden = true
            
            
            statusTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewApproval.frame.size.width - 10, height: viewApproval.frame.height - 20))
            statusTableView.register(UITableViewCell.self, forCellReuseIdentifier: "StatusCell")
            statusTableView.dataSource = self
            statusTableView.delegate = self
            statusTableView.separatorStyle = .none
            viewApproval.addSubview(statusTableView)
            
         //   dialogView.addSubview(viewApproval)
            
            footerLineView.frame = CGRect(x: 10, y: dateButton.frame.origin.y + 38, width: dialogViewWidth-20, height: 1)
            footerLineView.backgroundColor = UIColor.lightGray
            dialogView.addSubview(footerLineView)
            
            acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            acceptBtn.backgroundColor = UIColor.applyButtonColor
            acceptBtn.layer.cornerRadius = acceptBtn.frame.height / 2
            acceptBtn.setTitle("Apply", for: UIControl.State.normal)
            acceptBtn.addTarget(self, action: #selector(acceptBtnTapped), for: .touchUpInside)
            acceptBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            acceptBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            
            dialogView.addSubview(acceptBtn)
            
            cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2
            cancelBtn.backgroundColor = UIColor.appBGColor
            cancelBtn.setTitle("Cancel", for: UIControl.State.normal)
            cancelBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            cancelBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            cancelBtn.addTarget(self, action: #selector(cancelBtnTapped), for: .touchUpInside)
            dialogView.addSubview(cancelBtn)
            
            
            
            
            let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
            dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
            dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
            dialogView.backgroundColor = UIColor.white
            dialogView.layer.cornerRadius = 6
            addSubview(dialogView)
        }
        else {
            dialogView.clipsToBounds = true
            
            backgroundView.frame = frame
            backgroundView.backgroundColor = UIColor.black
            backgroundView.alpha = 0.6
            backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
            addSubview(backgroundView)
            
            dialogViewWidth = frame.width-64
            
            let labelFilter:UILabel = UILabel(frame: CGRect(x: 0, y: 10, width: dialogViewWidth, height: 30))
            labelFilter.text = "Filter List"
            labelFilter.textAlignment = .center
            labelFilter.font = UIFont.boldSystemFontOfSize(size: 15)
            dialogView.addSubview(labelFilter)
            
            let viewHeaderLine:UILabel = UILabel(frame: CGRect(x: 10, y: labelFilter.frame.origin.y + labelFilter.frame.height+2, width: dialogViewWidth-20, height: 1))
            viewHeaderLine.backgroundColor = UIColor.lightGray
            dialogView.addSubview(viewHeaderLine)
            
            dateButton.frame = CGRect(x: 10, y: viewHeaderLine.frame.origin.y + 8, width: dialogViewWidth * 0.4, height: 30)
            dateButton.setTitle("Date:", for: .normal)
            dateButton.titleLabel?.textAlignment = .left
            dateButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            dateButton.contentHorizontalAlignment = .left
            dateButton.setTitleColor(UIColor.black, for:.normal)
            //dateButton.addTarget(self, action: #sel, for: <#T##UIControl.Event#>)
            dateButton.addTarget(self, action: #selector(didDateTapped), for: .touchUpInside)
            dialogView.addSubview(dateButton)
            
            datePicker.datePickerMode = .date
            
            datePicker.addTarget(self, action: #selector(datePickervalueChanged(currentPicker:)), for: .valueChanged)
            
            viewDate.frame = CGRect(x: dialogViewWidth/2 - 10, y: dateButton.frame.origin.y, width: dialogViewWidth/2, height: 170)
            viewDate.backgroundColor = UIColor.clear
            viewDate.layer.cornerRadius = 5.0
            viewDate.layer.borderWidth = 1.0
            viewDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewDate.isHidden = true
            
            labelStartDate.frame = CGRect(x: 10, y: 10, width: viewDate.frame.width - 20, height: 30)
            labelStartDate.text = "Start Date"
            labelStartDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelStartDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelStartDate)
            
            imgvwStartDate.frame = CGRect(x: 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwStartDate.backgroundColor = UIColor.white
            imgvwStartDate.contentMode = .scaleAspectFill
            imgvwStartDate.image = UIImage(named: "date_icon")
            imgvwStartDate.clipsToBounds = true
            viewDate.addSubview(imgvwStartDate)
            
            lblStartDateTxt.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 10, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 30), height: 30)
            lblStartDateTxt.text = "12 Nov 2018"
            lblStartDateTxt.textColor = UIColor.black
            
            lblStartDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblStartDateTxt)
            
            txtFldStartDate.frame = CGRect(x: imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 5, y: labelStartDate.frame.origin.y + labelStartDate.frame.height + 3, width: viewDate.frame.width - (imgvwStartDate.frame.origin.x + imgvwStartDate.frame.width + 20), height: 30)
            txtFldStartDate.layer.cornerRadius = 5.0
            txtFldStartDate.layer.borderWidth = 1.0
            txtFldStartDate.tag = 10
            txtFldStartDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldStartDate.inputView = datePicker
            txtFldStartDate.inputAccessoryView = toolBar()
            txtFldStartDate.delegate = self
            //txtFldStartDate.forEach{$0.inputView = datePicker;$0.inputAccessoryView = toolBar()}
            viewDate.addSubview(txtFldStartDate)
            
            
            labelEndDate.frame = CGRect(x: 10, y: txtFldStartDate.frame.origin.y + txtFldStartDate.frame.height + 10, width: viewDate.frame.width - 20, height: 30)
            labelEndDate.text = "End Date"
            labelEndDate.font = UIFont.boldSystemFontOfSize(size: 14)
            labelEndDate.textColor = UIColor.filterDateColor
            viewDate.addSubview(labelEndDate)
            
            imgvwEndDate.frame = CGRect(x: 10, y: labelEndDate.frame.origin.y + labelStartDate.frame.height + 3, width: 25, height: 25)
            imgvwEndDate.backgroundColor = UIColor.white
            imgvwEndDate.contentMode = .scaleAspectFill
            imgvwEndDate.image = UIImage(named: "date_icon")
            imgvwEndDate.clipsToBounds = true
            viewDate.addSubview(imgvwEndDate)
            
            lblEndDateTxt.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 10, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 30), height: 30)
            lblEndDateTxt.text = "12 Nov 2018"
            lblEndDateTxt.textColor = UIColor.black
            lblEndDateTxt.font = UIFont().mainFont(ofSize: 12)
            viewDate.addSubview(lblEndDateTxt)
            
            txtFldEndDate.frame = CGRect(x: imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 5, y: labelEndDate.frame.origin.y + labelEndDate.frame.height + 3, width: viewDate.frame.width - (imgvwEndDate.frame.origin.x + imgvwEndDate.frame.width + 20), height: 30)
            txtFldEndDate.layer.cornerRadius = 5.0
            txtFldEndDate.layer.borderWidth = 1.0
            txtFldEndDate.tag = 11
            txtFldEndDate.inputView = datePicker
            txtFldEndDate.inputAccessoryView = toolBar()
            txtFldEndDate.layer.borderColor = UIColor.filterBorderColor.cgColor
            txtFldEndDate.delegate = self
            viewDate.addSubview(txtFldEndDate)
            
            
            dialogView.addSubview(viewDate)
            
            
            
            staffButton.frame = CGRect(x: 10, y: dateButton.frame.origin.y + 38, width: dialogViewWidth * 0.4, height: 30)
            staffButton.setTitle("Staff Name:", for: .normal)
            staffButton.contentHorizontalAlignment = .left
            staffButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            staffButton.setTitleColor(UIColor.black, for:.normal)
            staffButton.addTarget(self, action: #selector(didStaffTapped), for: .touchUpInside)
            dialogView.addSubview(staffButton)
            
            viewStaff.frame = CGRect(x: dialogViewWidth/2 - 10, y: staffButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewStaff.backgroundColor = UIColor.clear
            viewStaff.layer.cornerRadius = 5.0
            viewStaff.layer.borderWidth = 1.0
            viewStaff.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewStaff.isHidden = true
            
            staffTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewStaff.frame.size.width - 10, height: viewStaff.frame.height - 20))
            staffTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
            staffTableView.dataSource = self
            staffTableView.delegate = self
            viewStaff.addSubview(staffTableView)
            
            dialogView.addSubview(viewStaff)
            
            approvalButton.frame = CGRect(x: 10, y: staffButton.frame.origin.y + 38, width: dialogViewWidth * 0.4, height: 30)
            approvalButton.setTitle("Approval Status:", for: .normal)
            approvalButton.titleLabel?.textAlignment = .left
            approvalButton.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 14)
            approvalButton.contentHorizontalAlignment = .left
            approvalButton.setTitleColor(UIColor.black, for:.normal)
            approvalButton.addTarget(self, action: #selector(didApprovalTapped), for: .touchUpInside)
            dialogView.addSubview(approvalButton)
            
            viewApproval.frame = CGRect(x: dialogViewWidth/2 - 10, y: approvalButton.frame.origin.y, width: dialogViewWidth/2-30, height: 170)
            viewApproval.backgroundColor = UIColor.clear
            viewApproval.layer.cornerRadius = 5.0
            viewApproval.layer.borderColor = UIColor.filterBorderColor.cgColor
            viewApproval.layer.borderWidth = 1.0
            viewApproval.isHidden = true
            
            
            statusTableView = UITableView(frame: CGRect(x: 5, y: 10, width: viewApproval.frame.size.width - 10, height: viewApproval.frame.height - 20))
            statusTableView.register(UITableViewCell.self, forCellReuseIdentifier: "StatusCell")
            statusTableView.dataSource = self
            statusTableView.delegate = self
            statusTableView.separatorStyle = .none
            viewApproval.addSubview(statusTableView)
            
            dialogView.addSubview(viewApproval)
            
            footerLineView.frame = CGRect(x: 10, y: approvalButton.frame.origin.y + 38, width: dialogViewWidth-20, height: 1)
            footerLineView.backgroundColor = UIColor.lightGray
            dialogView.addSubview(footerLineView)
            
            acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            acceptBtn.backgroundColor = UIColor.applyButtonColor
            acceptBtn.layer.cornerRadius = acceptBtn.frame.height / 2
            acceptBtn.setTitle("Apply", for: UIControl.State.normal)
            acceptBtn.addTarget(self, action: #selector(acceptBtnTapped), for: .touchUpInside)
            acceptBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            acceptBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            
            dialogView.addSubview(acceptBtn)
            
            cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
                , width: 80, height: 30)
            cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2
            cancelBtn.backgroundColor = UIColor.appBGColor
            cancelBtn.setTitle("Cancel", for: UIControl.State.normal)
            cancelBtn.titleLabel?.font = UIFont.boldSystemFontOfSize(size: 15)
            cancelBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
            cancelBtn.addTarget(self, action: #selector(cancelBtnTapped), for: .touchUpInside)
            dialogView.addSubview(cancelBtn)
            
            
            
            
            let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
            dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
            dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
            dialogView.backgroundColor = UIColor.white
            dialogView.layer.cornerRadius = 6
            addSubview(dialogView)
        }
        
    }
    
    func toolBar() -> UIToolbar {
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        //UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneWithNumberPad)),
        return numberToolbar
    }
    
    @objc func doneWithNumberPad() {
        self.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.tintColor = .clear
        datePicker.tag = textField.tag
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    @objc func datePickervalueChanged(currentPicker: UIDatePicker) {
        print(datePicker.tag)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        print(formatter.string(from: currentPicker.date))
        if(currentPicker.tag == 10)
        {
            lblStartDateTxt.text = (formatter.string(from: currentPicker.date))
        }
        else if(currentPicker.tag == 11)
        {
            lblEndDateTxt.text = (formatter.string(from: currentPicker.date))
        }
       // btnDates[currentPicker.tag-1].enableMultilineText(buttonText:  String(format: "%@%@%@",currentPicker.tag == 1 ? "Start Date": "End Date", "\n", formatter.string(from: currentPicker.date)) , currentImage: "")
    }
    
    //MARK: - Tableview Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == staffTableView)
        {
        print("Num: \(indexPath.row)")
        print("Value: \(arrayStaff[indexPath.row])")
        }
        else
        {
            if(arrayStatus.contains(indexPath.row)) {
                arrayStatus.remove(indexPath.row)
                tableView.reloadData()
            }
            else
            {
                arrayStatus.add(indexPath.row)
                tableView.reloadData()
            }
            print("Num: \(indexPath.row)")
            print("Value: \(arrayStatusList[indexPath.row])")
            print("Value of Array :\(arrayStatus)")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == staffTableView) {
        return arrayStaff.count
        }
        else {
        return arrayStatusList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == staffTableView) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.selectionStyle = .none
        cell.textLabel!.text = "\(arrayStaff[indexPath.row])"
        cell.textLabel?.font = UIFont().mainFont(ofSize: 14)
        return cell
        }
        else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath as IndexPath)
        cell.selectionStyle = .none
//        cell.textLabel!.text = "\(arrayStatusList[indexPath.row])"
//        cell.textLabel?.font = UIFont().mainFont(ofSize: 14)
            
            let imgVwtick = UIImageView()
            imgVwtick.backgroundColor = UIColor.white
            imgVwtick.frame = CGRect(x: 10, y: 9, width: 25, height: 25)
            if(arrayStatus.contains(indexPath.row)) {
                imgVwtick.image = UIImage(named: "filterTick")
            }
            else {
                imgVwtick.image = UIImage(named: "filterUntick")
            }
            cell.addSubview(imgVwtick)
            
            let lblStatus = UILabel()
            lblStatus.frame = CGRect(x: imgVwtick.frame.width + imgVwtick.frame.origin.x + 5, y: 5, width: cell.frame.width - (imgVwtick.frame.width + imgVwtick.frame.origin.x + 10), height: 34)
            lblStatus.text = "\(arrayStatusList[indexPath.row])"
            lblStatus.textColor = UIColor.darkGray
            lblStatus.font = UIFont().mainFont(ofSize: 14)
            cell.addSubview(lblStatus)
           print(cell.frame.height)
        return cell
        }
    }
    
    
    
    func bottomView() {
        
        
        
    }
    @objc func didPeriodBtnTapped(){
        self.endEditing(true)
        periodView.isHidden = false
        viewDate.isHidden = true
        dateButton.frame = CGRect(x: dateButton.frame.origin.x, y: periodView.frame.origin.y + periodView.frame.size.height + 8, width: dateButton.frame.width, height: 30)
        footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: dateButton.frame.origin.y + dateButton.frame.size.height + 8, width: footerLineView.frame.width
            , height: 1)
        acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
        //dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
    }
    @objc func didDateTapped(){
        self.endEditing(true)
        viewDate.isHidden = false
        viewStaff.isHidden = true
        viewApproval.isHidden = true
        if(viewTitle == "Expense Claims" || viewTitle == "Budget")
        {
            staffButton.frame = CGRect(x: staffButton.frame.origin.x, y: viewDate.frame.origin.y + viewDate.frame.size.height + 8, width: staffButton.frame.width, height: 30)
            approvalButton.frame = CGRect(x: approvalButton.frame.origin.x, y: viewDate.frame.origin.y + viewDate.frame.size.height + 8, width: approvalButton.frame.width, height: 30)
            
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: approvalButton.frame.origin.y + approvalButton.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
            
            
        
        }
        else if(viewTitle == "Payroll") {
            periodView.isHidden = true
            periodButton.frame = CGRect(x: 10, y: viewHeaderLine.frame.origin.y + 8, width: dialogViewWidth * 0.4, height: 30)
            dateButton.frame = CGRect(x: dateButton.frame.origin.x, y: periodButton.frame.origin.y + periodButton.frame.size.height + 8, width: dateButton.frame.width, height: 30)
            viewDate.isHidden = false
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: viewDate.frame.origin.y + viewDate.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
        }
        else
        {
            staffButton.frame = CGRect(x: staffButton.frame.origin.x, y: viewDate.frame.origin.y + viewDate.frame.size.height + 8, width: staffButton.frame.width, height: 30)
            approvalButton.frame = CGRect(x: approvalButton.frame.origin.x, y: staffButton.frame.origin.y + staffButton.frame.size.height + 8, width: approvalButton.frame.width, height: 30)
            
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: approvalButton.frame.origin.y + approvalButton.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
        }
        
        
        acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
        //dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
        
    }
    @objc func didStaffTapped(){
        self.endEditing(true)
        viewDate.isHidden = true
        viewStaff.isHidden = false
        viewApproval.isHidden = true
        if(viewTitle == "Purchase Request") {
            staffButton.frame = CGRect(x: staffButton.frame.origin.x, y: dateButton.frame.origin.y , width: staffButton.frame.width, height: 30)
            approvalButton.frame = CGRect(x: approvalButton.frame.origin.x, y: viewStaff.frame.origin.y + viewStaff.frame.size.height + 8, width: approvalButton.frame.width, height: 30)
            
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: approvalButton.frame.origin.y + approvalButton.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
        }
        
        else {
            staffButton.frame = CGRect(x: staffButton.frame.origin.x, y: dateButton.frame.origin.y + dateButton.frame.size.height + 8, width: staffButton.frame.width, height: 30)
            approvalButton.frame = CGRect(x: approvalButton.frame.origin.x, y: viewStaff.frame.origin.y + viewStaff.frame.size.height + 8, width: approvalButton.frame.width, height: 30)
            
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: approvalButton.frame.origin.y + approvalButton.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
        }
        
        
        acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
      //  dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
    }
    @objc func didApprovalTapped(){
        self.endEditing(true)
        viewDate.isHidden = true
        viewStaff.isHidden = true
        viewApproval.isHidden = false
        
        if(viewTitle == "Purchase Request") {
            staffButton.frame = CGRect(x: staffButton.frame.origin.x, y: dateButton.frame.origin.y, width: staffButton.frame.width, height: 30)
            approvalButton.frame = CGRect(x: approvalButton.frame.origin.x, y: staffButton.frame.origin.y + staffButton.frame.size.height + 8, width: approvalButton.frame.width, height: 30)
            
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: viewApproval.frame.origin.y + viewApproval.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
        }
        else if(viewTitle == "Expense Claims" || viewTitle == "Budget") {
            staffButton.frame = CGRect(x: staffButton.frame.origin.x, y: dateButton.frame.origin.y + dateButton.frame.size.height + 8, width: staffButton.frame.width, height: 30)
            approvalButton.frame = CGRect(x: approvalButton.frame.origin.x, y: dateButton.frame.origin.y + dateButton.frame.size.height + 8, width: approvalButton.frame.width, height: 30)
            
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: viewApproval.frame.origin.y + viewApproval.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
        }
        else {
            staffButton.frame = CGRect(x: staffButton.frame.origin.x, y: dateButton.frame.origin.y + dateButton.frame.size.height + 8, width: staffButton.frame.width, height: 30)
            approvalButton.frame = CGRect(x: approvalButton.frame.origin.x, y: staffButton.frame.origin.y + staffButton.frame.size.height + 8, width: approvalButton.frame.width, height: 30)
            
            footerLineView.frame = CGRect(x: footerLineView.frame.origin.x, y: viewApproval.frame.origin.y + viewApproval.frame.size.height + 8, width: footerLineView.frame.width
                , height: 1)
        }
        
        
        acceptBtn.frame = CGRect(x: dialogViewWidth/2 - 100, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        cancelBtn.frame = CGRect(x: dialogViewWidth/2 + 10, y: footerLineView.frame.origin.y + footerLineView.frame.height + 8
            , width: 80, height: 30)
        
        let dialogViewHeight = cancelBtn.frame.origin.y + cancelBtn.frame.size.height + 10
     //   dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
    }
    @objc func didTappedOnBackgroundView(){
        dismiss(animated: true)
    }
    @objc func acceptBtnTapped(){
        dismiss(animated: true)
        guard let cb = completionBlock else {return}
        cb("Accept")
        
    }
    @objc func cancelBtnTapped(){
        dismiss(animated: true)
    }
    @objc private func buttonClicked(_ sender: UIButton) {
        if sender.tag == 10{ // approve
            didTappedOnBackgroundView()
            
        }else if sender.tag == 11{
            //cancel
            didTappedOnBackgroundView()
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
