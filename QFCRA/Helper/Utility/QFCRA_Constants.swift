//
//  QFCRA_Constants.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 21/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import Foundation
import UIKit

enum SegueName: String {
    case showDashBoard = "openMainVC"
    case openOTP = "OTPPage"
    case oTPtoDashVC = "DashVC"
    case openClaim = "openClaim"
    case prdetails = "prDetails"
    func getsegName() ->String { return self.rawValue }
}

struct QFCRA_SideMenu {
    static let QFCRA_PP =  "Personal Profile"
    static let QFCRA_Dash =  "Dashboard"
    static let QFCRA_LR =  "Leave Request"
    static let QFCRA_EC =  "Expense Claims"
    static let QFCRA_PR =  "Purchase Requisition"
    static let QFCRA_Bra =  "Budget"
    static let QFCRA_Payroll =  "Payroll"
    static let QFCRA_Logout =  "Logout"
}

struct Constants {
    static let screenSize = UIScreen.main.bounds
}
