//
//  QFCRA_Extensions.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 20/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import Foundation
import UIKit

let divident: CGFloat = 255.0
let alpha: CGFloat  = 1.0

// card views
var cornerRadius: CGFloat = 5
var shadowOffsetWidth: Int = 0
var shadowOffsetHeight: Int = 3
var shadowColor: UIColor? = .gray
var shadowOpacity: Float = 0.5

protocol SegueHandlerType {
    associatedtype SegueIdentifier: RawRepresentable
}

extension SegueHandlerType where Self: UIViewController,
    SegueIdentifier.RawValue == String
{
    
    func performSegueWithIdentifier(segueIdentifier: SegueIdentifier,
                                    sender: Any?) {
        performSegue(withIdentifier: segueIdentifier.rawValue, sender: sender)
    }
    
    func segueIdentifierForSegue(segue: UIStoryboardSegue) -> SegueIdentifier {
        
        // still have to use guard stuff here, but at least you're
        // extracting it this time
        guard let identifier = segue.identifier,
            let segueIdentifier = SegueIdentifier(rawValue: identifier) else {
                fatalError("Invalid segue identifier \(String(describing: segue.identifier)).") }
        
        return segueIdentifier
    }
}

extension UIViewController: StoryboardIdentifiable {
    func showAndHideNavigationBar(status: Bool){
        // Show & Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(status, animated: true)
    }
    
    func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func connectionSegue(currentName: String){
        performSegue(withIdentifier: currentName, sender: self)
    }
    
    func popToPreviousVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        
    }
    
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last ?? ""
    }
    
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

extension UIView{
    func enableCardView(){
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
   
    
   
}



extension UIColor{
    static let appBGColor = UIColor(red:166.0/divident, green:29.0/divident ,blue:55.0/divident, alpha:alpha)
    static let appGoldenColor = UIColor(red:189.0/divident, green:139.0/divident ,blue:54.0/divident, alpha:alpha)
    static let appPRbuttonColor = UIColor(red:213.0/divident, green:236.0/divident ,blue:divident/divident, alpha:alpha)
    static let filterTxtColor = UIColor(red:39.0/divident, green:39.0/divident ,blue:39.0/divident, alpha:alpha)
    static let filterBorderColor = UIColor(red:154.0/divident, green:197.0/divident ,blue:234.0/divident, alpha:alpha)
    static let applyButtonColor = UIColor(red:22.0/divident, green:160.0/divident ,blue:133.0/divident, alpha:alpha)
    static let filterDateColor = UIColor(red:128.0/divident, green:128.0/divident ,blue:128.0/divident, alpha:alpha)
    
}

extension UIFont {
    
    
    
    class func regularFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Century Gothic", size: size)!
    }
    //   class func lightSystemFontOfSize(size: CGFloat) -> UIFont {
    //    return UIFont.init(name: "Century Gothic-Light", size: size)!
    //    }
        class func boldSystemFontOfSize(size: CGFloat) -> UIFont {
            return UIFont(name: "CenturyGothic-Bold", size: size)!
        }
    
    private static func customFont(name: String, size: CGFloat) -> UIFont {
        let font = UIFont(name: name, size: size)
        assert(font != nil, "Can't load font: \(name)")
        return font ?? UIFont.systemFont(ofSize: size)
    }
    
    func mainFont(ofSize size: CGFloat) -> UIFont {
        return .customFont(name: "Century Gothic", size: size)
    }
}

extension UIButton {
    func enableCornerRadious(borderColor: UIColor, status: Bool){
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        if status{
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = 1.0
            self.layer.cornerRadius = 4
        }else{
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    func enableCardbuttons(){
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
    func enableMultilineText(buttonText: String, currentImage: String) {
        
        //applying the line break mode
        self.titleLabel?.lineBreakMode = .byWordWrapping
        if !currentImage.isEmpty{     self.setImage(UIImage(named: currentImage)!, for: .normal) }
        
        //var buttonText: NSString = "hello\nthere"
        
        //getting the range to separate the button title strings
        //  var newlineRange: NSRange = buttonText.range(of: "\n")
        
        //getting both substrings
        var substring1: String = ""
        var substring2: String = ""
        
        if buttonText.containsIgnoringCase(find: "\n"){
            let result = buttonText.components(separatedBy:"\n")
            if result.count > 1{  substring2 =  "\n" + result.last! } //"\n"
            substring1 = result.first!
        }
        
        //assigning diffrent fonts to both substrings
        let attrString =  NSMutableAttributedString(string: substring1, attributes: [ NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.regularFontOfSize(size: 13) ])
        
        let attrString1 =  NSMutableAttributedString(string: substring2, attributes: [ NSAttributedString.Key.foregroundColor: UIColor.appBGColor, NSAttributedString.Key.font: UIFont.boldSystemFontOfSize(size: 14) ])
        attrString.append(attrString1)
        //appending both attributed strings
        // attrString.appendAttributedString(attrString1)
        
        //assigning the resultant attributed strings to the button
        self.setAttributedTitle(attrString, for: .normal)
    }
}

extension NSAttributedString.Key {
    static let myName = NSAttributedString.Key(rawValue: "myCustomAttributeKey")
}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}


extension UITableView {
    
    func clearEmptyCell(){
        self.tableFooterView = UIView()
    }
}

extension UILabel
{
    func addImage(imageName: String)
    {
        let attachment:NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)
        
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        let myString:NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
        myString.append(attachmentString)
        
        self.attributedText = myString
    }
    
    func enableMultilineText(txt: String, regularFontSize: CGFloat, boldFontSize: CGFloat){
        //applying the line break mode
        self.lineBreakMode = .byWordWrapping
        self.numberOfLines = 0
        
        
        //getting the range to separate the button title strings
        //  var newlineRange: NSRange = buttonText.range(of: "\n")
        
        //getting both substrings
        var substring1: String = ""
        var substring2: String = ""
        var subString3: String = ""
        
        if txt.containsIgnoringCase(find: "\n"){
            let result = txt.components(separatedBy:"\n")
            if result.count > 2{  subString3 =  result.last!; substring2 = result[1] } else {  substring2 =   result.last!}
            substring1 = result.first!
        }
        
        //assigning diffrent fonts to both substrings
        let attrString =  NSMutableAttributedString(string: substring1, attributes: [ NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.regularFontOfSize(size: regularFontSize)])
        
        let attrString1 =  NSMutableAttributedString(string: substring2, attributes: [ NSAttributedString.Key.foregroundColor: UIColor.appBGColor, NSAttributedString.Key.font: UIFont.boldSystemFontOfSize(size: boldFontSize)])
        attrString.append(attrString1)
        
        if !subString3.isEmpty {
            let attrString2 =  NSMutableAttributedString(string: subString3, attributes: [ NSAttributedString.Key.foregroundColor: UIColor.appBGColor, NSAttributedString.Key.font: UIFont.regularFontOfSize(size: 13)])
            attrString.append(attrString2)
        }
        //appending both attributed strings
        // attrString.appendAttributedString(attrString1)
        
//        let style = NSMutableParagraphStyle()
//        style.lineSpacing = 2.0// change line spacing between paragraph like 36 or 48
//       // style.minimumLineHeight = 20 // change line spacing between each line like 30 or 40
//        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: txt.count))
    
        
//        let attributedString:NSMutableAttributedString
//        if let labelattributedText = self.attributedText {
//            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
//        } else {
//            attributedString = NSMutableAttributedString(string: labelText)
//        }
//
//        // Line spacing attribute
//        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
      //  self.attributedText = attributedString
        //assigning the resultant attributed strings to the button
        self.attributedText = attrString
    }
}


extension UITextField {
    
    enum Direction {
        case Left
        case Right
    }
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
    // add image to textfield
    func withImage(direction: Direction, imageName: String, colorSeparator: UIColor, colorBorder: UIColor){
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 45))
        mainView.layer.cornerRadius = 5
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 45))
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.layer.cornerRadius = 5
        view.layer.borderWidth = CGFloat(0.5)
        view.layer.borderColor = colorBorder.cgColor
        //   mainView.addSubview(view)
        
        let imageView = UIImageView(image: UIImage(named: imageName))
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 12.0, y: 10.0, width: 24.0, height: 24.0)
        mainView.addSubview(imageView)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = .clear // colorSeparator
        mainView.addSubview(seperatorView)
        
        if(Direction.Left == direction){ // image left
            seperatorView.frame = CGRect(x: 45, y: 0, width: 5, height: 45)
            self.leftViewMode = .always
            self.leftView = mainView
        } else { // image right
            seperatorView.frame = CGRect(x: 0, y: 0, width: 5, height: 45)
            self.rightViewMode = .always
            self.rightView = mainView
        }
        
        //  self.layer.borderColor = colorBorder.cgColor
        //  self.layer.borderWidth = CGFloat(0.5)
        //   self.layer.cornerRadius = 5
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 0.0
    }
    
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
            
        }
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.clear.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.appBGColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

extension UIApplication{
    
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}



extension UIStoryboard {
    enum Storyboard: String {
        case main
        
        var filename: String {
            return rawValue.capitalized
        }
    }
    
    // MARK: - Convenience Initializers
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.filename, bundle: bundle)
    }
    
    // MARK: - Class Functions
    class func storyboard(_ storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: storyboard.filename, bundle: bundle)
    }
    
    // MARK: - View Controller Instantiation from Generics
    func instantiateViewController<T: UIViewController>() -> T {
        guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }
        
        return viewController
    }
}

/*Tableview cell's textfield make next responder */
extension UITableView {
    
    func nextResponder(index: Int){
        var currIndex = -1
        for i in index+1..<index+100{
            if let view = self.superview?.superview?.viewWithTag(i){
                view.becomeFirstResponder()
                currIndex = i
                break
            }
        }
        
        let ind = IndexPath(row: currIndex - 100, section: 0)
        if let nextCell = self.cellForRow(at: ind){
            self.scrollRectToVisible(nextCell.frame, animated: true)
        }
    }
    
    func keyboardRaised(height: CGFloat){
        self.contentInset.bottom = height
        self.scrollIndicatorInsets.bottom = height
    }
    
    func keyboardClosed(){
        self.contentInset.bottom = 0
        self.scrollIndicatorInsets.bottom = 0
        self.scrollRectToVisible(CGRect.zero, animated: true)
    }
    
}
