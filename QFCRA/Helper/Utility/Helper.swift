//
//  Helper.swift
//  XTRM
//
//  Created by Karthikeyan A. on 12/01/18.
//  Copyright © 2018 DCI. All rights reserved.
//

import Foundation
import UIKit


class AppUtils{
    class func appUtilityPush(_ CurrentVC : UIViewController, _ newVC : UIViewController) {
//        let transition = CATransition()
//        transition.duration = 0.65
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.moveIn //kCATransitionFade
//        transition.subtype = CATransitionSubtype.fromRight
//
//        CurrentVC.navigationController?.view.layer.add(transition, forKey: nil)
        CurrentVC.navigationController?.pushViewController(newVC, animated: false)
    }
    class func navigationBarTint(control: UIViewController) {
        let navigationBar = control.navigationController?.navigationBar
        navigationBar?.barTintColor = .appBGColor
        navigationBar?.isTranslucent = false
        navigationBar?.tintColor = UIColor.white
    }
    
    class func appUtilityPresent(_ CurrentVC : UIViewController ,  _ newVC : UIViewController) {
        CurrentVC.present(newVC, animated: true, completion: nil)
    }
    
    //show alert
    static func displayAlert(title: String, message: String, cancel: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        if !cancel.isEmpty
        {
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertController.addAction(cancelAction)
        }
        
        guard let viewController = UIApplication.getTopMostViewController() else {
            // fatalError("find TopVC")
            return
        }
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    // validate Email
}


