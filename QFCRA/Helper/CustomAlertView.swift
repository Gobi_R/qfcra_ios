//
//  CustomAlertView.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 02/12/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class CustomAlertView: UIView, Modal {
    
    

    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnApprove: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
     var backgroundView = UIView()
     var dialogView = UIView()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    convenience init(title:String,image:UIImage) {
        
        self.init(frame: UIScreen.main.bounds)
        dialogView.clipsToBounds = true
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
        addSubview(backgroundView)
      //  initialize(title: title, image: image)
        
    }
   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func btnHandleACtion(_ sender: UIButton) {
        if sender.tag == 10{ // approve
            
        }else if sender.tag == 11{
            //cancel
             didTappedOnBackgroundView()
        }
    }
   @objc func didTappedOnBackgroundView(){
        dismiss(animated: true)
    }
}
