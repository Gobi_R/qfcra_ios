//
//  ClaimsViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 29/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class claimsCell: UITableViewCell{
    
    @IBOutlet weak var viewBGRequest: UIView!{
        didSet{
            viewBGRequest.enableCardView()
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet var btnViewRequest: [UIButton]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnViewRequest.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true) }
        
    }
    
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
        for index in 0...btn1Text.count - 1{
            btnViewRequest[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
        }
    }
}


class ClaimsViewController: MenuDisplayControllerViewController{
    
    @IBOutlet var btnClaims: [UIButton]!
    @IBOutlet var btnMyClaims: [UIButton]!
    @IBOutlet var btnMyTeamSubClaims: [UIButton]!
    
    @IBOutlet weak var tblMyTeamClaims: UITableView!{
        didSet{
            tblMyTeamClaims.clearEmptyCell()
            tblMyTeamClaims.rowHeight = 177.0
        }
    }
    
    @IBOutlet weak var tblReferalBonus: UITableView!{
        didSet{
            tblReferalBonus.clearEmptyCell()
            tblReferalBonus.rowHeight = 177.0
        }
    }
    
    @IBOutlet weak var tblpersonalExpense: UITableView!{
        didSet{
            tblpersonalExpense.clearEmptyCell()
            tblpersonalExpense.rowHeight = 177.0
        }
    }
    
    @IBOutlet weak var tblBusinesslExpense: UITableView!{
        didSet{
            tblBusinesslExpense.clearEmptyCell()
            tblBusinesslExpense.rowHeight = 177.0
        }
    }
    @IBOutlet weak var tblMyteamReferalBonus: UITableView! {
        didSet {
            tblMyteamReferalBonus.clearEmptyCell()
            tblMyteamReferalBonus.rowHeight = 177.0
        }
    }
    @IBOutlet weak var tblMyteamPersonalExpense: UITableView! {
        didSet {
            tblMyteamPersonalExpense.clearEmptyCell()
            tblMyteamPersonalExpense.rowHeight = 177.0
        }
    }
    @IBOutlet weak var tblMyteamBusinessExpense: UITableView! {
        didSet {
            tblMyteamBusinessExpense.clearEmptyCell()
            tblMyteamBusinessExpense.rowHeight = 177.0
        }
    }
    
    @IBAction func rightBarButtonTapped(_ sender: Any) {
        let alert = CustomFilter(title: "Expense Claims")
        alert.show(animated: true)
    }
    
    
    var isMyclaim = true
    let screenWidth = UIScreen.main.bounds.size.width
    let screenHeight = UIScreen.main.bounds.size.height
    var commontableView: UITableView!
    
 //   @IBOutlet weak var scrollMyClaims: UIScrollView!
    @IBOutlet weak var scrollClaims: UIScrollView!
    let cellIddentifier  = "claimsCell"
    
    var scrollOrientation: UIImage.Orientation?
    var lastContentOffset: CGFloat = 0.0
    var lastPos = CGPoint.zero
    
    var innerOrientation: UIImage.Orientation?
    var innerContentOffset: CGFloat = 0.0
    var inneros = CGPoint.zero
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewMyTeamTop: UIView!
    
    var isfirstload = true
    
    let demoTitle = ["Application #\nRA-12345", "Amount\n3000" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        // Do any additional setup after loading the view.
    }
    // MARK: - side menu
    @IBAction func openSideMenu(_ sender: UIBarButtonItem) {
        openSideMenu()
    }
    
    // MARK: - enable Scrollview position
    func updateUI(){
        DispatchQueue.main.async {
            
            //self.scrollClaims.translatesAutoresizingMaskIntoConstraints = false
            //self.tblMyTeamClaims.translatesAutoresizingMaskIntoConstraints = false
            self.viewTop.enableCardView()
             self.viewMyTeamTop.enableCardView()
            
            var vieLeaveDetails = self.btnClaims.last!.frame
            vieLeaveDetails.origin.y = ( UIApplication.shared.statusBarFrame.size.height ) +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
            self.btnClaims.last!.frame = vieLeaveDetails
            
            var vieLeave = self.btnClaims.first!.frame
            vieLeave.origin.y = ( UIApplication.shared.statusBarFrame.size.height ) +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
            self.btnClaims.first!.frame = vieLeave
            
            var viewTop = self.viewTop.frame
            viewTop.size.width = self.screenWidth
            viewTop.origin.y =   0
            self.viewTop.frame = viewTop
            
            var viewMyTeamTop = self.viewMyTeamTop.frame
            viewMyTeamTop.size.width = self.screenWidth
            viewMyTeamTop.origin.y =   0
            viewMyTeamTop.origin.x = self.screenWidth
            self.viewMyTeamTop.frame = viewMyTeamTop
            
            var getScroll = self.scrollClaims.frame
            getScroll.size.width = self.screenWidth
           // getScroll.origin.y =   self.btnClaims.last!.frame.origin.y + self.btnClaims.last!.frame.size.height
            getScroll.origin.y =   self.btnClaims.last!.frame.origin.y + self.btnClaims.last!.frame.size.height
            
            self.scrollClaims.frame = getScroll
            
            
            var tblMyRequest = self.tblMyTeamClaims.frame
            tblMyRequest.size.width = self.screenWidth
            tblMyRequest.origin.x = self.screenWidth
            tblMyRequest.size.height = self.scrollClaims.frame.size.height
            tblMyRequest.origin.y = self.viewMyTeamTop.frame.height
            self.tblMyTeamClaims.frame = tblMyRequest
            //self.tblMyTeamClaims.isHidden = true
            
            var tblMyteamPersonalExpense = self.tblMyteamPersonalExpense.frame
            tblMyteamPersonalExpense.size.width = self.screenWidth
            tblMyteamPersonalExpense.origin.x =  self.screenWidth * 2
            tblMyteamPersonalExpense.origin.y = 0
            tblMyteamPersonalExpense.size.height = self.scrollMyClaims.frame.size.height
            self.tblMyteamPersonalExpense.frame = tblMyteamPersonalExpense
            
            var tblMyteamReferalBonus = self.tblMyteamReferalBonus.frame
            tblMyteamReferalBonus.size.width = self.screenWidth
            tblMyteamReferalBonus.origin.y = 0
            tblMyteamReferalBonus.origin.x =  self.screenWidth * 3
            tblMyteamReferalBonus.size.height = self.scrollMyClaims.frame.size.height
            self.tblReferalBonus.frame = tblMyteamReferalBonus
            
            var scrollMyClaims = self.scrollMyClaims.frame
            scrollMyClaims.size.width = self.screenWidth
            scrollMyClaims.origin.x = 0
            scrollMyClaims.origin.y =  self.viewTop.frame.origin.y + self.viewTop.frame.size.height
            scrollMyClaims.size.height = self.scrollClaims.frame.origin.x + self.scrollClaims.frame.size.height - 5
            self.scrollMyClaims.frame = scrollMyClaims
            
            // self.scrollClaims.addSubview(self.scrollMyClaims)
            
            var tblBusinesslExpense = self.tblBusinesslExpense.frame
            tblBusinesslExpense.size.width = self.screenWidth
            tblBusinesslExpense.origin.x =  0
            tblBusinesslExpense.origin.y = 0
            tblBusinesslExpense.size.height = self.scrollMyClaims.frame.size.height
            self.tblBusinesslExpense.frame = tblBusinesslExpense
            
            var tblpersonalExpense = self.tblpersonalExpense.frame
            tblpersonalExpense.size.width = self.screenWidth
            tblpersonalExpense.origin.x =  self.screenWidth
            tblpersonalExpense.origin.y = 0
            tblpersonalExpense.size.height = self.scrollMyClaims.frame.size.height
            self.tblpersonalExpense.frame = tblpersonalExpense
            
            var tblReferalBonus = self.tblReferalBonus.frame
            tblReferalBonus.size.width = self.screenWidth
            tblReferalBonus.origin.y = 0
            tblReferalBonus.origin.x =  self.screenWidth * 2
            tblReferalBonus.size.height = self.scrollMyClaims.frame.size.height
            self.tblReferalBonus.frame = tblReferalBonus
            
            self.tblReferalBonus.bringSubviewToFront(self.btnAdd)
            self.tblpersonalExpense.bringSubviewToFront(self.btnAdd)
            self.tblpersonalExpense.bringSubviewToFront(self.btnAdd)
            
            self.scrollMyClaims.bringSubviewToFront(self.btnAdd)
            self.scrollClaims.bringSubviewToFront(self.btnAdd)
            
            // self.scrollRequest.addSubview(self.tblMyTeam)
            
            //            var gettteamLeaveTbl = self.tblTeamleave.frame
            //            gettteamLeaveTbl.size.width = self.screenWidth
            //            gettteamLeaveTbl.origin.x =  self.screenWidth * 2
            //            gettteamLeaveTbl.size.height = self.scrollRequest.frame.size.height
            //            self.tblTeamleave.frame = gettteamLeaveTbl
            self.scrollClaims.contentSize = CGSize(width: self.screenWidth * 4, height: self.scrollClaims.frame.size.height)
            self.scrollMyClaims.contentSize = CGSize(width: self.screenWidth * 3, height: self.scrollMyClaims.frame.size.height)
            self.updateInnerCornerradious(sender: self.btnMyClaims.last!)
            self.updateCornerradious(sender: self.btnClaims.first!)
            self.scrollClaims.isScrollEnabled = false
            self.scrollMyClaims.isScrollEnabled = false
        }
    }
    
    // MARK: - handle Request
    @IBAction func handleClaims(_ sender: UIButton) {
        sender.tag == 1 ? (isMyclaim = true) : (isMyclaim = false)
        updateCornerradious(sender: sender)
    }
    
    @IBAction func handleMyClaims(_ sender: UIButton) {
        updateInnerCornerradious(sender: sender)
    }
    
    @IBAction func handleMySubClaims(_ sender: UIButton) {
        updateMyTeamsubItems(sender: sender)
    }
    
    // MARK: - open claims
    @IBAction func btnOpenAddClaims(_ sender: UIButton) {
        //          let wizard1: AddClaimViewController = UIStoryboard(storyboard: .main).instantiateViewController()
        //        AppUtils.appUtilityPush(self, wizard1)
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ClaimsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIddentifier, for: indexPath) as! claimsCell
        cell.updateuserData(btn1Text: demoTitle)
        
        // cell.updateuserData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        commontableView = tableView
//        if tableView == tblMyTeamClaims {
        if isMyclaim == true {
            connectionSegue(currentName: SegueName.openClaim.getsegName())
            UserDefaults.standard.set("my", forKey: "team")
        } else {
//        }  else {
             connectionSegue(currentName: SegueName.openClaim.getsegName())
            UserDefaults.standard.set("myteam", forKey: "team")
//        }
        }
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueName.openClaim.getsegName(),commontableView == tblMyTeamClaims{
//            UserDefaults.standard.set("My team", forKey: "detail")
//            let Claimdetail = ClaimdetailTableViewController()
//            Claimdetail.pageFrom = "team"
        }
//            let nextScene = segue.destination as? ClaimdetailTableViewController {
//               print ("comes here")
//                if {
//                    print ("comes here")
//                }
//        }
        
        }
   
    
}

extension ClaimsViewController:  UIScrollViewDelegate{
    
    // MARK: Scrollview delegates
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView.tag == 2{
            self.lastContentOffset = scrollView.contentOffset.x
        }else{
            self.innerContentOffset = scrollView.contentOffset.x
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.tag == 2{
            //  scrollDirectionDetermined = false
            if self.lastContentOffset < scrollView.contentOffset.x {
                // moved right
           //     movebuttonsShadows(scrollView: scrollView)
            } else if self.lastContentOffset > scrollView.contentOffset.x {
                // moved left
           //     movebuttonsShadows(scrollView: scrollView)
            }else{
                
            }
        }else{
            
            if self.innerContentOffset < scrollView.contentOffset.x {
                // moved right
           //     movebuttonsShadows(scrollView: scrollView)
            } else if self.innerContentOffset > scrollView.contentOffset.x {
                // moved left
          //      movebuttonsShadows(scrollView: scrollView)
            }else{
                
            }
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.tag == 1{
            innerOrientation = scrollView.contentOffset.y > inneros.y ? .down : .up
            inneros = scrollView.contentOffset
        }else{
            scrollOrientation = scrollView.contentOffset.y > lastPos.y ? .down : .up
            lastPos = scrollView.contentOffset
        }
    }
    
    func movebuttonsShadows(scrollView: UIScrollView){
        let pageWidth: CGFloat = scrollView.frame.size.width
        if scrollView.tag == 2{
            // team claim
            let page: Int = Int(floor((scrollView.contentOffset.x - pageWidth / 4) / pageWidth) + 1)
            handleTeamClaims(page: page)
           // updateCornerradious(sender: page == 0 ? self.btnClaims.first! : page == 1 ? self.btnClaims.last! : page == 2 ? self.btnMyTeamSubClaims[1] : self.btnMyTeamSubClaims.last)
        }else{
            // my claims
            let page: Int = Int(floor((scrollView.contentOffset.x - pageWidth / 3) / pageWidth) + 1)
            print("\(page)")
            updateInnerCornerradious(sender: page == 0 ? self.btnMyClaims.first! : page == 1 ? self.btnMyClaims[1] :self.btnMyClaims.last!)
        }
    }
    //MARK: - Class Methods
    func setContentOffset(position: CGFloat){
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
                self.scrollClaims.contentOffset.x = position
            }, completion: nil)
        }
    }
    
    func setinnerContentOffset(position: CGFloat){
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
                self.scrollMyClaims.contentOffset.x = position
            }, completion: nil)
        }
    }
    
    func updateInnerCornerradious(sender: UIButton){
        for getBut in btnMyClaims{
            getBut.setTitleColor(.lightGray, for: .normal)
            //            if let getTag = getBut.viewWithTag(101) {
            //                getTag.removeFromSuperview()
            //            }
        }
        //        let lineView = UIView(frame: CGRect(x: 0, y: sender.frame.size.height, width: sender.frame.size.width, height: 2))
        //        lineView.backgroundColor = UIColor.white
        //        lineView.tag = 101
        //        sender.addSubview(lineView)
        sender.setTitleColor(.black, for: .normal)
        //   print("sender.tag==\(sender.tag)")
        print("TAG \(sender.tag)")
        if sender.tag == 3{
            self.scrollMyClaims.setContentOffset(.zero, animated: true)
        }else if  sender.tag == 4{
            self.scrollMyClaims.setContentOffset(CGPoint(x: self.screenWidth, y: 0), animated: true)
        }
        else  {
            self.scrollMyClaims.setContentOffset(CGPoint(x: self.screenWidth * 2, y: 0), animated: true)
        }
    }
    
    func handleTeamClaims(page: Int){
        if page < 2{
            updateCornerradious(sender: page == 0 ? self.btnClaims.first! :  self.btnClaims.last!)
        }else{
            updateMyTeamsubItems(sender: page == 2 ? self.btnMyTeamSubClaims[1] : self.btnMyTeamSubClaims.last! )
            
        }
        //print("PAGES \(page)")
        
    }
    
    func updateMyTeamsubItems(sender: UIButton){
        for getBut in btnMyTeamSubClaims{
            getBut.setTitleColor(.lightGray, for: .normal)
            //            if let getTag = getBut.viewWithTag(101) {
            //                getTag.removeFromSuperview()
            //            }
        }
        //        let lineView = UIView(frame: CGRect(x: 0, y: sender.frame.size.height, width: sender.frame.size.width, height: 2))
        //        lineView.backgroundColor = UIColor.white
        //        lineView.tag = 101
        //        sender.addSubview(lineView)
        sender.setTitleColor(.black, for: .normal)
        //   print("sender.tag==\(sender.tag)")
        
        if sender.tag == 3{
            self.scrollMyClaims.setContentOffset(.zero, animated: true)
            
        }else if  sender.tag == 4{
            self.scrollMyClaims.setContentOffset(CGPoint(x: self.screenWidth, y: 0), animated: true)
        }
        else  {
            self.scrollMyClaims.setContentOffset(CGPoint(x: self.screenWidth * 2, y: 0), animated: true)
        }
    }
    
    func updateCornerradious(sender: UIButton){
        for getBut in btnClaims{
            getBut.setTitleColor(.lightGray, for: .normal)
            //            if let getTag = getBut.viewWithTag(101) {
            //                getTag.removeFromSuperview()
            //            }
        }
        //        let lineView = UIView(frame: CGRect(x: 0, y: sender.frame.size.height, width: sender.frame.size.width, height: 2))
        //        lineView.backgroundColor = UIColor.white
        //        lineView.tag = 101
        //        sender.addSubview(lineView)
        sender.setTitleColor(.white, for: .normal)
        
        
        if sender.tag == 1{
            self.scrollClaims.setContentOffset(.zero, animated: true)
        }else if  sender.tag == 2{
            //self.scrollClaims.setContentOffset(CGPoint(x: self.screenWidth, y: 0), animated: true)
            self.scrollClaims.setContentOffset(.zero, animated: true)

        }
        //        else  {
        //            self.scrollClaims.setContentOffset(CGPoint(x: self.screenWidth * 2, y: 0), animated: true)
        //        }
    }
}
