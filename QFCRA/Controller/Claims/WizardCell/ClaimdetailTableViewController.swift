//
//  ClaimWizar4TableViewController.swift
//  QFCRA
//
//  Created by Gobi R. on 01/12/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class ClaimdetailTableViewController: AccordionTableViewController {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var btnHeader: [UIButton]!
    @IBOutlet var vieReject: UIView!
    @IBOutlet weak var btnCancelOutlet: UIButton! {
        didSet {
            btnCancelOutlet.enableCornerRadious(borderColor: .clear, status: false)
        }
    }
    @IBOutlet var vwCancelView: UIView!
    
    let screenSize = UIScreen.main.bounds
    var pageFrom = ""
//    let titleArray = ["Staff File #\n102800", "Application #\n123456", "Request Amount\n24576",  "Approved Amount\n24576"]
//    let imageArray = ["prname_icon", "prname_icon", "funds",  "funds"]
   
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        for index in 0...titleArray.count - 1{
//            btnHeader[index].enableMultilineText(buttonText: titleArray[index], currentImage: imageArray[index])
//            btnHeader[index].enableCornerRadious(borderColor: .appPRbuttonColor, status: true)
//        }
       /// print("pageFrom==\(pageFrom)")
       
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        expandFirstCell()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let getValue =  UserDefaults.standard.object(forKey: "team") as? String, !getValue.isEmpty, getValue == "myteam" {
            DispatchQueue.main.async {
                self.vieReject.frame =  CGRect(x: 0, y: self.screenSize.height - 44 - self.getsafeAreaBottomMargin(), width: self.screenSize.width, height: 44)
                self.vieReject.tag = 122
                UIApplication.shared.keyWindow?.addSubview(self.vieReject)
                UIApplication.shared.keyWindow?.bringSubviewToFront(self.vieReject)
            }
        } else {
            if let getValue = UserDefaults.standard.object(forKey: "team") as? String, !getValue.isEmpty,getValue == "my" {
                self.vwCancelView.frame =  CGRect(x: 0, y: self.screenSize.height - 44 - self.getsafeAreaBottomMargin(), width: self.screenSize.width, height: 44)
                self.vwCancelView.tag = 123
                UIApplication.shared.keyWindow?.addSubview(self.vwCancelView)
                UIApplication.shared.keyWindow?.bringSubviewToFront(self.vwCancelView)
            }
        }
            // if !pageFrom.isEmpty{
            
       // }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
         if let getValue =  UserDefaults.standard.object(forKey: "team") as? String, !getValue.isEmpty, getValue == "myteam"{
      // if !pageFrom.isEmpty{
             UserDefaults.standard.removeObject(forKey: "team")
             UIApplication.shared.keyWindow?.viewWithTag(122)?.removeFromSuperview()
         } else {
            if let getValue = UserDefaults.standard.object(forKey: "team") as? String, !getValue.isEmpty,getValue == "my" {
                UserDefaults.standard.removeObject(forKey: "team")
                UIApplication.shared.keyWindow?.viewWithTag(123)?.removeFromSuperview()
            }
        }
    }
    
    func getsafeAreaBottomMargin() -> CGFloat {
        if #available(iOS 11.0, *) {
            let currentwindow = UIApplication.shared.windows.first
            return (currentwindow?.safeAreaLayoutGuide.owningView?.frame.size.height)! - (currentwindow?.safeAreaLayoutGuide.layoutFrame.size.height)! - (currentwindow?.safeAreaLayoutGuide.layoutFrame.origin.y)!
        }
        else {
            return 0
        }
    }
    
    @IBAction func handleApprove(_ sender: UIButton) {
        let alert = CustomAlert(title: sender.currentTitle! )
        alert.show(animated: true)
        UserDefaults.standard.set(alert.dialogView.frame.origin.y, forKey: "dialogFrame")
    }
    
    @IBAction func btnDismiss(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    func expandFirstCell() {
        let firstCellIndexPath = IndexPath(row: 0, section: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }
    
    @IBAction func btnCancelMyClaimAction(_ sender: UIButton) {
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellWizard4", for: indexPath) as! ClaimsWizard4Cell
        cell.btnIopneAttachment.tag = indexPath.row
        cell.btnIopneAttachment.addTarget(self, action: #selector(openAttachment(_:)), for: .touchUpInside)
        cell.btnIopneAttachment1.tag = indexPath.row
        cell.btnIopneAttachment1.addTarget(self, action: #selector(openAttachment(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func openAttachment(_ sender: UIButton) {
        showAlert(title: "", message: "This will be implemented at the time of technical implementation.", actionTitles: ["Okay"])
    }
    
    func showAlert(title: String, message: String, actionTitles : [String]){
        self.popupAlert(title: title, message: message, actionTitles: actionTitles, actions:[{action1 in
            
            }, nil])
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 460.0 : 45.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 250.0
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
