//
//  AddClaimViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 29/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class AddClaimViewController: UIViewController {
    // wizard count
    @IBOutlet var btnWizard: [UIButton]!
    // intial count of wizard
    var globalWizardCount  = 0
    // next and previous button
    @IBOutlet var btnNextandPrevious: [UIButton]!
    @IBOutlet weak var vieBottom: UIView!
    let screenWidth = UIScreen.main.bounds.size.width
    @IBOutlet weak var scrollWizard: UIScrollView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        btnWizard.forEach { $0.enableCornerRadious(borderColor: .clear, status: true) }
        btnNextandPrevious.forEach { $0.enableCornerRadious(borderColor: .clear, status: false) }
        handleNextandPrevious(btnNextandPrevious.first! )
        loadChildViewcontroller()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - load wizard controller
    func loadChildViewcontroller(){
        DispatchQueue.main.async {
            let wizard1: ClaimWizard1ViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            let wizard2: Claim2TableViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            let wizard3: Claim3TableViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            let wizard4: ClaimWizar4TableViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            
            var idx:Int = 0
            for viewController in [wizard1, wizard2, wizard3,wizard4] {
                
                self.addChild(viewController)
                let originX:CGFloat = CGFloat(idx) * self.screenWidth
                viewController.view.frame = CGRect(x: originX, y: 0, width: self.screenWidth, height: self.scrollWizard.frame.size.height)
                self.scrollWizard!.addSubview(viewController.view)
                viewController.didMove(toParent: self)
                idx += 1
            }
        }
        
    }
    
    // MARK: - handle Wizards
    @IBAction func handleWizard(_ sender: UIButton) {
        // get current select wizard index
        globalWizardCount = sender.tag - 1
        updateColor()
    }
    // MARK: - update color
    func updateColor(){
        // clear all color initially
        for getbut in btnWizard {
            getbut.backgroundColor = .lightGray
            getbut.alpha = 0.3
        }
        // change color upto selectd Index
        for index in 0...globalWizardCount {
            btnWizard[index].backgroundColor = .appBGColor
            btnWizard[index].alpha = 1.0
        }
        self.scrollWizard.setContentOffset(CGPoint(x: self.screenWidth * CGFloat(globalWizardCount), y: 0), animated: true)
        
    }
    
    // MARK: - btnHandle Previous and next
    @IBAction func handleNextandPrevious(_ sender: UIButton) {
        if sender.currentTitle == "Submit"{
            self.navigationController?.popViewController(animated: true)
            return
        }
        //
        btnNextandPrevious.first!.isHidden = false
        btnNextandPrevious.last?.setTitle("Next", for: .normal)
        // if both button available
        var getlastBtn = btnNextandPrevious.last!.frame
        getlastBtn.origin.x  =  btnNextandPrevious.first!.frame.origin.x + btnNextandPrevious.first!.frame.size.width + 20
        btnNextandPrevious.last!.frame = getlastBtn
        // previous
        if sender.tag == 101 {
            // decrement by 1
            globalWizardCount -= 1
            // less than 0 reset 0
            if globalWizardCount  < 0{ globalWizardCount = 0}
        }else {
            // next action
            //increment by 1
            globalWizardCount += 1
            // greater than wizard count reset max wizard count
            if globalWizardCount  > 3 { globalWizardCount = 3}
        }
        // if first wizard hide the previous button and change the position of next button at middle of page
        if globalWizardCount == 0{
            // first
            btnNextandPrevious.first!.isHidden = true
            getlastBtn.origin.x  =  vieBottom.frame.size.width / 2 - 30
            btnNextandPrevious.last!.frame = getlastBtn
            btnNextandPrevious.last!.center = CGPoint(x: vieBottom.bounds.midX, y: vieBottom.bounds.midY)
          //  btnNextandPrevious.last!.center = vieBottom.center
            
        }else if globalWizardCount == 3 {
            // last wizard change the title to submit action
            btnNextandPrevious.last?.setTitle("Submit", for: .normal)
        }
        // update the wizard color
       updateColor()
       // handleWizard(btnWizard[globalWizardCount])
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
