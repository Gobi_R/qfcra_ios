//
//  ClaimWizard1ViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 30/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit


class ClaimsWizard1Cell: UITableViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnExpense: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
    
}


class ClaimWizard1ViewController: UIViewController {
    
    @IBOutlet weak var tblwizard1: UITableView!{
        didSet {
            tblwizard1.clearEmptyCell()
            tblwizard1.rowHeight = 60.0
        }
    }
    let msg = "Dynamic forms will be implemented at the time of technical implementation as the data is completely controlled in ERP."
    let cellIdentifier  = "wizard1"
    
    let claimName = ["Personal Expenses", "Business Expenses", "Referral Bonus"]
    let paymentType = ["Against Salary", "Cash", "Card"]

    override func viewDidLoad() {
        super.viewDidLoad()
        showAlert(title: "", message: msg, actionTitles: ["Ok"])
    }
    func showAlert(title: String, message: String, actionTitles : [String]){
        self.popupAlert(title: title, message: message, actionTitles: actionTitles, actions:[{action1 in
            
            }, nil])
    }
}

extension ClaimWizard1ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 || indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ClaimsWizard1Cell
            cell.lblTitle.text  = indexPath.row == 0 ? "Claim Name" : "Payment Type"
            cell.btnExpense.setTitle(indexPath.row == 0 ? "Referreal Bonus" : "Against Salary", for: .normal)
             cell.btnExpense.tag = indexPath.row
             cell.btnExpense.addTarget(self, action: #selector(btnappendClaimType), for: .touchUpInside)

            return cell
        } else  {
           let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileCell
            cell.textLabel?.text = indexPath.row == 1 ? "Application #" : indexPath.row == 2 ? "Status" : "Claim Date"
            cell.detailTextLabel?.text = indexPath.row == 1 ? "RA 003470" : indexPath.row == 2 ? "Not Submitted" : "10 Oct 2018"
         
            return cell
        }
    }
    
    @objc func btnappendClaimType(_ sender: UIButton) {
        alert(msg: sender.tag == 0 ? claimName : paymentType , sender: sender)
    }
    func alert(msg: [String], sender: UIButton) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        for item in msg{
            let superbutton = UIAlertAction(title: item , style: .default, handler: { (action) in
                 sender.setTitle(action.title, for: .normal)
            })
            alertController.addAction(superbutton)
        }
        //alertController.view.tag = currentTag
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
