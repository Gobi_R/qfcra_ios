//
//  Claim3TableViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 30/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class ClaimsWizard3Cell: AccordionTableViewCell, UITextFieldDelegate {
    
    
    //    @IBOutlet weak var lblTitle: UILabel!
    //    @IBOutlet weak var btnExpense: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var arrow: UIImageView! {
        didSet {
            arrow.tintColor = .appBGColor
        }
    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        nextTextField?(textField.tag)
//        return textField.resignFirstResponder()
//    }
    
    @IBOutlet weak var btnAttachment: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // MARK: Override
    override func setExpanded(_ expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)
       
         if animated {
         let alwaysOptions: UIView.AnimationOptions = [.allowUserInteraction,
         .beginFromCurrentState,
         .transitionCrossDissolve]
         let expandedOptions: UIView.AnimationOptions = [.transitionFlipFromTop, .curveEaseOut]
         let collapsedOptions: UIView.AnimationOptions = [.transitionFlipFromBottom, .curveEaseIn]
         let options = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
         
         UIView.transition(with: detailView, duration: 0.3, options: options, animations: {
         self.toggleCell()
         }, completion: nil)
         } else {
         toggleCell()
         }
    }
    
    // MARK: Helpers
    private func toggleCell() {
       // detailView.isHidden = !expanded
        arrow.transform = expanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
    }
}

class Claim3TableViewController: AccordionTableViewController {
    
    private var days = ["Monday"]
    let cellIdentidfifer = "cellWizard3"
       let claimIDe = ["Educational Assistance", "EGate", "Goverment Health Card"]
    
    @IBOutlet var viewForHeader: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.clearEmptyCell()
        expandFirstCell()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 325.0 : 50.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return days.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentidfifer, for: indexPath) as! ClaimsWizard3Cell
        // cell.updateuserData()
        cell.btnClose.tag = indexPath.row
        cell.btnClose.addTarget(self, action: #selector(closeBtnAction(_:)), for: .touchUpInside)
        cell.btnAttachment.tag = indexPath.row
        cell.btnAttachment.addTarget(self, action: #selector(openAttachment(_:)), for: .touchUpInside)
//        cell.nextTextField = { [weak self] (tag) in
//            guard let strongSelf = self else{
//                return
//            }
//
//            strongSelf.formTableView.nextResponder(index: tag)
//        }
        return cell
    }
    
     @objc func openAttachment(_ sender: UIButton) {
         showAlert(title: "", message: "This will be implemented at the time of technical implementation.", actionTitles: ["Okay"])
    }
    
    func showAlert(title: String, message: String, actionTitles : [String]){
        self.popupAlert(title: title, message: message, actionTitles: actionTitles, actions:[{action1 in
            }, nil])
    }
    
    func expandFirstCell() {
        let firstCellIndexPath = IndexPath(row: days.count-1, section: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }
    
    @IBAction func btnClaimName(_ sender: UIButton) {
        alert(msg:  claimIDe , sender: sender)
    }
    
    func alert(msg: [String], sender: UIButton) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        for item in msg{
            let superbutton = UIAlertAction(title: item , style: .default, handler: { (action) in
                sender.setTitle(action.title, for: .normal)
            })
            alertController.addAction(superbutton)
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        //alertController.view.tag = currentTag
        self.present(alertController, animated: true, completion: nil)
    }
    //Insert Wizard
    @IBAction func btnAddWizard() {
        if !days.isEmpty, days.count > 5{
            return
        }
        days.append("newText")
        self.tableView.reloadData()
        //        self.tableView.beginUpdates()
        //        self.tableView.reloadRows(at:  [IndexPath(row: days.count , section: 0)], with: .automatic)
        //        self.tableView.endUpdates()
        
    }
    
    
    @objc func closeBtnAction(_ sender: UIButton) {
        if !days.isEmpty, days.count < 2{
            showAlert(title: "", message: "You need to have at least one claim Id filled in.", actionTitles: ["Ok"])
            return
        }
        let indexPath = IndexPath(row: sender.tag, section: 0)
        self.days.remove(at: indexPath.row)
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
        self.tableView.endUpdates()
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewForHeader
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 130.0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var footerView : UIView?
        footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50) )
        footerView?.backgroundColor = .clear
        let dunamicButton = UIButton(type:.custom)
        //        dunamicButton.backgroundColor = .appBGColor
        dunamicButton.setImage(UIImage(named: "plus"), for: .normal)
        dunamicButton.frame = CGRect(x: tableView.frame.width - 50, y: 10, width: 30, height: 30)
        dunamicButton.addTarget(self, action: #selector(btnAddWizard), for: .touchUpInside)
        footerView?.addSubview(dunamicButton)
        return footerView
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }    
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


