//
//  ClaimWizar4TableViewController.swift
//  QFCRA
//
//  Created by Gobi R. on 01/12/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class ClaimsWizard4Cell: AccordionTableViewCell {
    @IBOutlet weak var detailView: UIView!
   
    @IBOutlet weak var viewSupplementary: UIView!{
        didSet{
            viewSupplementary.layer.masksToBounds = false
            viewSupplementary.layer.cornerRadius = 3; // if you like rounded corners
            viewSupplementary.layer.shadowOffset = CGSize(width: -0.2, height: 0.2) //%%% this shadow will hang slightly down and to the right
            viewSupplementary.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
            viewSupplementary.layer.shadowOpacity = 0.2;
             viewSupplementary.layer.borderWidth = 1/2
             viewSupplementary.layer.borderColor = UIColor.lightGray.cgColor
            
            
            
        }
    }
    
    @IBOutlet weak var viewSubView1: UIView!{
        didSet{
            self.viewSubView1.layer.cornerRadius = 10
            self.viewSubView1.layer.borderColor = UIColor.appPRbuttonColor.cgColor
            self.viewSubView1.layer.borderWidth = 1
            self.viewSubView1.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var viewSubview2: UIView!{
        didSet{
            self.viewSubview2.layer.cornerRadius = 10
            self.viewSubview2.layer.borderColor = UIColor.appPRbuttonColor.cgColor
            self.viewSubview2.layer.borderWidth = 1
            self.viewSubview2.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var arrow: UIImageView! {
        didSet {
            arrow.tintColor = .appBGColor
        }
    }
    
    @IBOutlet weak var btnIopneAttachment: UIButton!
    @IBOutlet weak var btnIopneAttachment1: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // MARK: Override
    
    override func setExpanded(_ expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)
        
        if animated {
            UIView.transition(with: detailView, duration: 0.3, animations: {
                self.toggleCell()
            }, completion: nil)
        } else {
            toggleCell()
        }
        /*
         if animated {
         let alwaysOptions: UIView.AnimationOptions = [.allowUserInteraction,
         .beginFromCurrentState,
         .transitionCrossDissolve]
         let expandedOptions: UIView.AnimationOptions = [.transitionFlipFromTop, .curveEaseOut]
         let collapsedOptions: UIView.AnimationOptions = [.transitionFlipFromBottom, .curveEaseIn]
         let options = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
         
         UIView.transition(with: detailView, duration: 0.3, options: options, animations: {
         self.toggleCell()
         }, completion: nil)
         } else {
         toggleCell()
         }
         */
    }
    
    // MARK: Helpers
    
    private func toggleCell() {
        // detailView.isHidden = !expanded
        arrow.transform = expanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
    }
}

class ClaimWizar4TableViewController: AccordionTableViewController {
    @IBOutlet var headerView: UIView!
    
    @IBOutlet var btnHeader: [UIButton]!
    let titleArray = ["Staff File #\n102800", "Application #\n123456", "Request Amount\n24576",  "Approved Amount\n24576"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for index in 0...titleArray.count - 1{
            btnHeader[index].enableMultilineText(buttonText: titleArray[index], currentImage: "")
             btnHeader[index].enableCornerRadious(borderColor: .appPRbuttonColor, status: true)
        }
        
       

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        expandFirstCell()
    }
    
    func expandFirstCell() {
        let firstCellIndexPath = IndexPath(row: 0, section: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellWizard4", for: indexPath) as! ClaimsWizard4Cell
        cell.btnIopneAttachment.tag = indexPath.row
        cell.btnIopneAttachment.addTarget(self, action: #selector(openAttachment(_:)), for: .touchUpInside)
        cell.btnIopneAttachment1.tag = indexPath.row
        cell.btnIopneAttachment1.addTarget(self, action: #selector(openAttachment(_:)), for: .touchUpInside)
       
        return cell
    }
    
    @objc func openAttachment(_ sender: UIButton) {
        showAlert(title: "", message: "This will be implemented at the time of technical implementation.", actionTitles: ["Okay"])
    }
    
    func showAlert(title: String, message: String, actionTitles : [String]){
        self.popupAlert(title: title, message: message, actionTitles: actionTitles, actions:[{action1 in
            
            }, nil])
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 460.0 : 45.0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 250.0
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
