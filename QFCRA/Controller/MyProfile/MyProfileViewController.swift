//
//  MyProfileViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 23/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell{
    
    override func layoutSubviews() {
        let size: CGSize = bounds.size
        super.layoutSubviews()
        
        textLabel?.frame  = CGRect(x: 50.0, y: textLabel?.frame.origin.y ?? 4.0, width: textLabel?.frame.size.width ?? 160, height: textLabel?.frame.size.height ?? 40)
        textLabel?.contentMode = .scaleAspectFit
        
        detailTextLabel?.frame  = CGRect(x: (size.width / 2) + 20, y: detailTextLabel?.frame.origin.y ?? 4.0, width: detailTextLabel?.frame.size.width ?? 160 , height: detailTextLabel?.frame.size.height ?? 40)
        detailTextLabel?.contentMode = .scaleAspectFit
        
    }
}



class MyProfileViewController: MenuDisplayControllerViewController {
    
    @IBOutlet weak var tblProfile: UITableView!{
        didSet{
            tblProfile.clearEmptyCell()
            tblProfile.rowHeight = 60.0
        }
    }
    let profileCellIdentifier = "profileCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let containerView:UIView = UIView(frame:CGRect(x: 10, y: 100, width: 300, height: 60 * 6))
        containerView.frame = tblProfile.bounds
       // self.tableView = UITableView(frame: containerView.bounds, style: .plain)
        containerView.backgroundColor = UIColor.clear
//        containerView.layer.shadowColor = UIColor.darkGray.cgColor
//        containerView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
//        containerView.layer.shadowOpacity = 1.0
//        containerView.layer.shadowRadius = 2
        
        self.tblProfile.layer.cornerRadius = 10
        self.tblProfile.layer.borderColor = UIColor.appPRbuttonColor.cgColor
        self.tblProfile.layer.borderWidth = 1.0/2
        self.tblProfile.layer.masksToBounds = true
//        self.view.addSubview(containerView)
//        containerView.addSubview(self.tblProfile)
    }
     // MARK: - open Side menu
    
    @IBAction func openSideMenu(_ sender: UIBarButtonItem) {
        openSideMenu()
    }
    override func viewDidLayoutSubviews(){
        tblProfile.frame = CGRect(x: tblProfile.frame.origin.x, y: tblProfile.frame.origin.y, width: tblProfile.frame.size.width, height: 60*6)
        tblProfile.reloadData()
    }
    
}

extension MyProfileViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as! ProfileCell
        var title  = ""
        var subTitle  = ""
        if indexPath.row == 0{
            title = "Staff File #"
            subTitle = "123456"
            
        }else if indexPath.row == 1{
            title = "Name"
            subTitle = "Demo user"
            
        }else if indexPath.row == 2{
            title = "Job"
            subTitle = "Associate"
            
        }else if indexPath.row == 3{
            title = "Qatar ID"
            subTitle = "461234579"
            
        }else if indexPath.row == 4{
            title = "Email ID"
            subTitle = "demo@gmail.com"
            
        }else if indexPath.row == 5{
            title = "Mobile Number"
            subTitle = "12345678"
            
        }
        
        
        
        cell.textLabel?.text = String(format:"%@:", title)
        cell.detailTextLabel?.text = subTitle
        return cell
        
    }
    
    
}
