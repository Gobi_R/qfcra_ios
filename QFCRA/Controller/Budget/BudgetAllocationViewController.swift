//
//  BudgetAllocationViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 27/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class BudgetAllocationCell: UITableViewCell{
    
    @IBOutlet weak var viewBGRequest: UIView!{
        didSet{
            viewBGRequest.enableCardView()
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var btnViewRequest: [UIButton]!
    @IBOutlet weak var lblApprovalStatus: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var btnViewAttachment: UIButton!
    @IBOutlet weak var btnApprove: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnViewRequest.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true) }
        
    }
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
        for index in 0...btn1Text.count - 1{
            btnViewRequest[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
        }
    }
}

class BudgetAllocationViewController: MenuDisplayControllerViewController {
    
    @IBOutlet weak var tblBudget: UITableView!{
        didSet {
            tblBudget.clearEmptyCell()
            tblBudget.rowHeight = 320.0
        }
    }
    
    @IBAction func rightBarButtonTapped(_ sender: Any) {
        let alert = CustomFilter(title: "Budget")
        alert.show(animated: true)
        UserDefaults.standard.set(alert.dialogView.frame.origin.y, forKey: "dialogFrame")

    }
    
    let demoTitle = ["Number\n123456", "Code\nBudget CF", "Type\nCarry Forward" ,"Date\n26 Nov 2018" ]
     let myRqIentifier  = "cellBudget"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - side menu
    @IBAction func openSideMenu(_ sender: UIBarButtonItem) {
        openSideMenu()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BudgetAllocationViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: myRqIentifier, for: indexPath) as! BudgetAllocationCell
        cell.updateuserData(btn1Text: demoTitle)
        cell.btnApprove.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
        cell.btnReject.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
        cell.btnViewAttachment.addTarget(self, action: #selector(openAttachment(_:)), for: .touchUpInside)
        

            return cell
        
    }
    
    @objc private func buttonClicked(_ sender: UIButton) {
        let alert = CustomAlert(title: sender.currentTitle ?? "Approve")
        alert.show(animated: true)
    }
    
    @objc private func openAttachment(_ sender: UIButton) {
        showAlert(title: "", message: "This will be implemented at the time of technical implementation.", actionTitles: ["Okay"])
    }
    
    func showAlert(title: String, message: String, actionTitles : [String]){
        self.popupAlert(title: title, message: message, actionTitles: actionTitles, actions:[{action1 in
            
            }, nil])
    }
    
//
    
    
    
}
