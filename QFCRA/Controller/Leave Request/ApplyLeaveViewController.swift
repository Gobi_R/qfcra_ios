//
//  ApplyLeaveViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 27/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import Foundation
import UIKit

class ApplyLeaveViewController: UIViewController {
    
    
    @IBOutlet var btnDates: [UIButton]!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet var txtDatePicker: [UITextField]!
    @IBOutlet weak var btncancelLeaveRequest: UIButton!
    
    var getPageFrom = ""
    let datePicker = UIDatePicker()
    let durationArray = ["Full Day","Half Day"]
    let leaveTypeArray = ["sick","Casual","Emergency","Vocaional"]
    
    @IBOutlet weak var btnSick: UIButton!
    @IBOutlet weak var btnDay: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
         hideKeyboardWhenTappedAround()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickervalueChanged(currentPicker:)), for: .valueChanged)
         btnDates.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true) }
        if !getPageFrom.isEmpty{
            btnSubmit.setTitle("Resubmit",for: .normal)
            btnSubmit.tag = 16
        }
        txtDatePicker.forEach{$0.inputView = datePicker;$0.inputAccessoryView = toolBar()}
        btnSubmit.enableCornerRadious(borderColor: .clear, status: false)
        btncancelLeaveRequest.enableCornerRadious(borderColor: .clear, status: false)
        btnDates[1].enableMultilineText(buttonText:   "End Date\n24 Nov 2018", currentImage: "")
        btnDates.first!.enableMultilineText(buttonText:   "Start Date\n24 Nov 2018",  currentImage: "")
 
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.removeObservers()
    }
     // MARK: - handle Submit action
    @IBAction func submitAction(_ sender: UIButton) {
        if sender.tag == 15 {
            popToPreviousVC()
        }else{
            // navigate to leave Request page
            if let vc = navigationController?.viewControllers.filter({ $0 is LeaveRequestViewController }).first {
                navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
    func toolBar() -> UIToolbar {
    let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
    numberToolbar.barStyle = .default
    numberToolbar.items = [
    
    UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
    UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))]
    numberToolbar.sizeToFit()
        //UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneWithNumberPad)),
    return numberToolbar
    }
    
//    @objc func cancelNumberPad() {
//        //Cancel with number pad
//        self.view.endEditing(true)
//    }
    @objc func doneWithNumberPad() {
        self.view.endEditing(true)
    }
    
    @objc func datePickervalueChanged(currentPicker: UIDatePicker) {
        print(datePicker.tag)
    let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        btnDates[currentPicker.tag-1].enableMultilineText(buttonText:  String(format: "%@%@%@",currentPicker.tag == 1 ? "Start Date": "End Date", "\n", formatter.string(from: currentPicker.date)) , currentImage: "")
    }
       // String(format: "%@%@%@",currentPicker.tag == 1 ? "Start Date": "End Date", "\n", formatter.string(from: currentPicker.date))
    @IBAction func btnTypeandDuration(_ sender: UIButton) {
        alert(msg: sender.tag == 4 ? durationArray : leaveTypeArray , currentTag: sender.tag)
         }
    
    func alert(msg: [String], currentTag: Int) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        for item in msg{
            let superbutton = UIAlertAction(title: item , style: .default, handler: { (action) in
                if currentTag == 4 {
                    self.btnDay.setTitle(action.title, for: .normal)}
                else{
                       self.btnSick.setTitle(action.title, for: .normal)}
                
            })
            alertController.addAction(superbutton)
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.view.tag = currentTag
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func cancelLeaveRequest(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*Observers for keyboard Events*/
    @objc func keyboardDidShow(notification: Notification) {
        if let userInfo = notification.userInfo as? [String: Any], !userInfo.isEmpty {
            if let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                let keyboardHeight = keyboardFrame.height + 40
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        self.view.frame.origin.y = 0
    }
}

extension ApplyLeaveViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.tintColor = .clear
        datePicker.tag = textField.tag
        return true
    }
}
