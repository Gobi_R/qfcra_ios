//
//  EditLeaveViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 28/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class EditLeaveViewController: UIViewController {
    
    @IBOutlet weak var tblEditLeave: UITableView!{
        didSet{
            tblEditLeave.clearEmptyCell()
            tblEditLeave.rowHeight = 55.0
        }
    }
    
    override func viewDidLayoutSubviews(){
        tblEditLeave.frame = CGRect(x: tblEditLeave.frame.origin.x, y: tblEditLeave.frame.origin.y, width: tblEditLeave.frame.size.width, height: 55 * (getType == "team" ? 9  :  getType == "payroll" ? 6 : 7))
        tblEditLeave.reloadData()
    }
    
    @IBOutlet weak var viewMyLeave: UIView!
    @IBOutlet var viewTeamLeave: UIView!
    @IBOutlet var viewEditbtn: UIView!
    
    var getType = ""
    
    let profileCellIdentifier = "profileCell"
    @IBOutlet weak var btnReCall: UIButton!
    
    @IBOutlet var btnLeave: [UIButton]!
    @IBOutlet weak var btnReject: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         btnLeave.forEach {
            $0.enableCornerRadious(borderColor: .clear, status: false)
           }
        // Do any additional setup after loading the view.
        viewTeamLeave.isHidden = true
        btnReCall.isHidden = false
        var getTitle = "LEAVE"
        
        self.tblEditLeave.layer.cornerRadius = 10
        self.tblEditLeave.layer.borderColor = UIColor.appPRbuttonColor.cgColor
        self.tblEditLeave.layer.borderWidth = 1.0
        self.tblEditLeave.layer.masksToBounds = true
        
        if !getType.isEmpty {
            viewTeamLeave.isHidden = false
            btnReCall.isHidden = true
            if   getType == "payroll"{
                getTitle = "PAYROLL"
            }
        }
        self.title = String(format: "%@ DETAIL",getTitle )
        btnReject.backgroundColor = UIColor(red: 29/155, green: 52/255, blue: 94/255, alpha: 1.0)
        
    }
    
    
    // MARK: - btn Recall
    @IBAction func btnRecallAction(_ sender: UIButton) {
        if sender.tag == 1{ // recall
            sender.tag = 2
            sender.setTitle("Edit", for: .normal)
        }else if sender.tag == 2{
            let applyLeave: ApplyLeaveViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            applyLeave.getPageFrom = "edit"
            AppUtils.appUtilityPush(self, applyLeave)
        }else if sender.tag == 10{
            // open approve action
            let alert = CustomAlert(title:"Approve" )
            alert.show(animated: true)
            UserDefaults.standard.set(alert.dialogView.frame.origin.y, forKey: "dialogFrame")
        }else if sender.tag == 20{
            // open Reject action
            let alert = CustomAlert(title:"Reject" )
            alert.show(animated: true)
            UserDefaults.standard.set(alert.dialogView.frame.origin.y, forKey: "dialogFrame")
        }
        
        
    }
    
     // MARK: - approve Reject action
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditLeaveViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getType == "team" ? 9  :  getType == "payroll" ? 6 : 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as! ProfileCell
         var title  = ""
         var subTitle  = ""
        if  getType == "payroll" {
            cell.contentView.viewWithTag(100)?.removeFromSuperview()
            self.viewTeamLeave.isHidden = false
            self.viewEditbtn.isHidden = true
            if indexPath.row == 0 {
                title = "Period Id"
                subTitle = "Employee0834"
                
            }else if indexPath.row == 1{
                title = "Process Desc"
                subTitle = "Salary Process"
                
            }else if indexPath.row == 2{
                title = "From Date"
                subTitle = "22 Oct 2018"
                
            }
            else if indexPath.row == 3{
                title = "To Date"
                subTitle = "24 Oct 2018"
                
            }else if indexPath.row == 4{
                title = "No of Employees"
                subTitle = "3"
    
            }else if indexPath.row == 5{
                title = "Attachment"
                subTitle = ""
                let imageView = UIImageView(image: UIImage(named: "Createnewclaim_icon")!)
                imageView.frame = CGRect(x: cell.detailTextLabel?.frame.origin.x ?? cell.contentView.frame.size.width/2, y: cell.contentView.frame.size.height/2 - 10, width: 25, height: 25)
                imageView.tag = 100
                imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openImage)))
                cell.contentView.addSubview(imageView)
            }
            cell.textLabel?.text = String(format:"%@:", title)
            cell.detailTextLabel?.text = subTitle
        } else if getType == "team"{
            self.viewTeamLeave.isHidden = false
            self.viewEditbtn.isHidden = true
            cell.contentView.viewWithTag(100)?.removeFromSuperview()
            if indexPath.row == 0 {
                title = "Staff Name"
                subTitle = "123456"
            } else if indexPath.row == 1 {
                title = "Staff File #"
                subTitle = "15621"
            } else if indexPath.row == 2 {
                title = "Requsted Date"
                subTitle = "16 Nov 2018"
            } else if indexPath.row == 3 {
                title = "Duration"
                subTitle = "5"
            }else if indexPath.row == 4{
                title = "From Date"
                subTitle = "22 Oct 2018"
            }
            else if indexPath.row == 5{
                title = "To Date"
                subTitle = "24 Oct 2018"
                
            }else if indexPath.row == 6{
                title = "Leave Type"
                subTitle = "Sick"
                
            }else if indexPath.row == 7{
                title = "Comments"
                subTitle = "None"
                
            }else if indexPath.row == 8{
                title = "Attachment"
                subTitle = ""
                let imageView = UIImageView(image: UIImage(named: "Createnewclaim_icon")!)
                imageView.frame = CGRect(x: cell.detailTextLabel?.frame.origin.x ?? cell.contentView.frame.size.width/2, y: cell.contentView.frame.size.height/2 - 10, width: 25, height: 25)
                imageView.tag = 100
                imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openImage)))
                cell.contentView.addSubview(imageView)
                //cell.detailTextLabel?.addImage(imageName: "Createnewclaim_icon")
            }
            cell.textLabel?.text = String(format:"%@:", title)
            cell.detailTextLabel?.text = subTitle
        } else if getType == "history" {
          cell.contentView.viewWithTag(100)?.removeFromSuperview()
            self.viewTeamLeave.isHidden = true
            self.btnReCall.isHidden = false
            if indexPath.row == 0 {
                title = "Application Id"
                subTitle = "123456"
            } else if indexPath.row == 1 {
                title = "Leave Type"
                subTitle = "Emergency"
            } else if indexPath.row == 2 {
                title = "Date"
                subTitle = "16 Nov 2018"
            } else if indexPath.row == 3 {
                title = "Days"
                subTitle = "5"
            } else if indexPath.row == 4 {
                    title = "Workflow Status"
                    subTitle = "Status"
                }
            else if indexPath.row == 5 {
                title = "Remarks"
                subTitle = "None"
            }else if indexPath.row == 6{
                title = "Attachment"
                let imageView = UIImageView(image: UIImage(named: "Createnewclaim_icon")!)
                imageView.frame = CGRect(x: cell.detailTextLabel?.frame.origin.x ?? cell.contentView.frame.size.width/2, y: cell.contentView.frame.size.height/2 - 10, width: 25, height: 25)
                imageView.tag = 100
                imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openImage)))
                cell.contentView.addSubview(imageView)
                //cell.detailTextLabel?.addImage(imageName: "Createnewclaim_icon")
            }
            cell.textLabel?.text = String(format:"%@:", title)
            cell.detailTextLabel?.text = subTitle
        }
        
        return cell
    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
//        if getType == "history" {
//            footerView.addSubview(viewEditbtn)
//            return footerView
//        } else {
//            footerView.addSubview(viewTeamLeave)
//            return footerView
//        }
     //   return getType == "history" ? (footerView.addSubview(viewEditbtn)) : (footerView.addSubview(viewTeamLeave))
//    }
    
    @objc func openImage(){
        showAlert(title: "", message: "This will be implemented at the time of technical implementation.", actionTitles: ["Okay"])
    }
    
    func showAlert(title: String, message: String, actionTitles : [String]){
        self.popupAlert(title: title, message: message, actionTitles: actionTitles, actions:[{action1 in
            
            }, nil])
    }
    
    
}

