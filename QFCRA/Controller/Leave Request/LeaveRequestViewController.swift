//
//  LeaveRequestViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 27/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class LeaveHistoryCell: UITableViewCell{
    
    @objc var tapButton: ((LeaveHistoryCell) -> Void)?
    
    @IBOutlet weak var viewBGRequest: UIView!{
        didSet{
            viewBGRequest.enableCardView()
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var btnViewRequest: [UIButton]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnViewRequest.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true)} //; $0.addTarget(self, action: #selector(setter: tapButton), for: .touchUpInside) }
        
    }
//    @objc func btnDetailsClick(sender: UIButton) {
//        tapButton?(self)
//    }
    
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
//        self.lblTitle.text = title
//        self.lblSubTitle.text = subTitle
        for index in 0...btn1Text.count - 1{
            btnViewRequest[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
        }
    }
}

class SummaryCell: UITableViewCell{
    
   
    @IBOutlet weak var lblEntitled: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
          textLabel?.frame = CGRect(x: 20, y: textLabel?.frame.origin.y ?? 0.0, width: textLabel?.frame.size.width ?? 0.0, height: textLabel?.frame.size.height ?? 0.0)
        lblEntitled?.frame = CGRect(x: frame.size.width/2 - 50, y: lblEntitled?.frame.origin.y ?? 0.0, width: lblEntitled.frame.size.width, height: lblEntitled?.frame.size.height ?? 0.0)
        lblEntitled.textAlignment = .center
        
        detailTextLabel?.frame = CGRect(x: (detailTextLabel?.frame.origin.x ?? 0.0)  - 50  , y: detailTextLabel?.frame.origin.y ?? 0.0, width: frame.size.width, height: detailTextLabel?.frame.size.height ?? 0.0)
        detailTextLabel?.textAlignment = .left
       
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        
    }
    
}

class TeamLeavesCell: UITableViewCell{
    
    @IBOutlet weak var viewBGRequest: UIView!{
        didSet{
            viewBGRequest.enableCardView()
            
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var btnViewRequest: [UIButton]!
    @IBOutlet weak var lbluserName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnViewRequest.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true) }
    }
//
//    func updateTeamData(){
//        self.lblTitle.text = "name"
//        self.lblSubTitle.text = "Abdul"
//        self.btnViewRequest.first?.enableMultilineText(buttonText: "name\nAbdul", currentImage: "idcard_icon")
//        self.btnViewRequest.last?.enableMultilineText(buttonText: "name\nDCI", currentImage: "idcard_icon")
//    }
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
        //        self.lblTitle.text = title
        //        self.lblSubTitle.text = subTitle
        for index in 0...btn1Text.count - 1{
            btnViewRequest[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
        }
    }
}

class LeaveRequestViewController: MenuDisplayControllerViewController {

    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var tblHistory: UITableView!{
        didSet {
            tblHistory.clearEmptyCell()
            tblHistory.rowHeight = 200.0
        }
    }
    
    @IBOutlet weak var tblSummary: UITableView!{
        didSet {
            tblSummary.clearEmptyCell()
            tblSummary.rowHeight = 61.0
        }
    }
    
    
    @IBOutlet weak var tblTeamleave: UITableView!{
        didSet {
            tblTeamleave.clearEmptyCell()
            tblTeamleave.rowHeight = 300.0
        }
    }
    
    @IBOutlet weak var scrollRequest: UIScrollView!
    @IBOutlet var managerSheet: [UIButton]!
    
    @IBOutlet weak var vieLeaveDetails: UIView!
    @IBOutlet weak var viewSummary: UIView!
    @IBOutlet weak var btnAddLeave: UIButton!
    
    
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var tableViewAlert: UITableView!
    
    let screenWidth = UIScreen.main.bounds.size.width
    let screenHeight = UIScreen.main.bounds.size.height
    
    let summaryIentifier  = "summary"
    let historyIentifier  = "history"
    let teamLeaveIentifier  = "teamLeave"
    
    var scrollOrientation: UIImage.Orientation?
    var lastContentOffset: CGFloat = 0.0
    var lastPos = CGPoint.zero
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
//        tableViewAlert.frame = CGRect(x: tableViewAlert.frame.origin.x, y: tableViewAlert.frame.origin.y, width: tableViewAlert.frame.size.width, height: tableViewAlert.contentSize.height)
//        viewAlert.frame = CGRect(x: viewAlert.frame.origin.x, y: viewAlert.frame.origin.y, width: viewAlert.frame.width, height: tableViewAlert.frame.size.height + 110)
    }
    @IBAction func rightBarButtonTapped(_ sender: Any) {
//        print("Right bar tapped")
//        viewFilter.isHidden = false
        let alert = CustomFilter(title: "")
        alert.show(animated: true)
    }
    
    @IBAction func buttonCloseTapped(_ sender: Any) {
        viewFilter.isHidden = true
    }
    
    //MARK: - Filter View
    @IBOutlet weak var filterHeaderLabel: UILabel!
    
    @IBOutlet weak var filterLine: UILabel!
    
    @IBOutlet weak var dateButton: UIButton!
    
    @IBOutlet weak var buttonStaff: UIButton!
    
    @IBOutlet weak var buttonApproval: UIButton!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var tblStaff: UITableView!
    @IBOutlet weak var viewApproval: UIView!
    @IBAction func btnDateTapped(_ sender: Any) {
        
        dateView.isHidden = false
    buttonStaff.frame = CGRect(x: buttonStaff.frame.origin.x, y: dateView.frame.origin.y + dateView.frame.height + 8, width: buttonStaff.frame.width, height: buttonStaff.frame.height)
        
        tblStaff.isHidden = true
        viewApproval.isHidden = true
        
        
        buttonApproval.frame = CGRect(x: buttonApproval.frame.origin.x, y: buttonStaff.frame.origin.y + buttonStaff.frame.height + 8, width: buttonApproval.frame.width, height: buttonApproval.frame.height)
        
    }
    
    @IBAction func btnStaffTapped(_ sender: Any) {
        dateView.isHidden = true
        tblStaff.isHidden = false
        viewApproval.isHidden = true
        
        buttonStaff.frame = CGRect(x: buttonStaff.frame.origin.x, y: dateButton.frame.origin.y + dateButton.frame.height + 8, width: buttonStaff.frame.width, height: buttonStaff.frame.height)
        
    }
    @IBAction func btnApprovalTapped(_ sender: Any) {
    }
    
    // MARK: - enable Scrollview position
    func updateUI(){
        DispatchQueue.main.async {
            
            self.scrollRequest.translatesAutoresizingMaskIntoConstraints = false
            self.tblHistory.translatesAutoresizingMaskIntoConstraints = false
            self.tblSummary.translatesAutoresizingMaskIntoConstraints = false
            self.tblTeamleave.translatesAutoresizingMaskIntoConstraints = false
            self.vieLeaveDetails.translatesAutoresizingMaskIntoConstraints = false
            
            var vieLeaveDetails = self.vieLeaveDetails.frame
            vieLeaveDetails.origin.y = ( UIApplication.shared.statusBarFrame.size.height ) +
                       (self.navigationController?.navigationBar.frame.height ?? 0.0)
             self.vieLeaveDetails.frame = vieLeaveDetails
            
            
            var getScroll = self.scrollRequest.frame
            getScroll.size.width = self.screenWidth
            getScroll.origin.y = self.vieLeaveDetails.frame.origin.y + self.vieLeaveDetails.frame.size.height
            // getScroll.origin.y = self.viewSummary.frame.origin.y + self.viewSummary.frame.size.height
            self.scrollRequest.frame = getScroll
            
            
            var tblMyRequest = self.tblHistory.frame
            tblMyRequest.size.width = self.screenWidth
            tblMyRequest.origin.x = 0
            tblMyRequest.size.height = self.scrollRequest.frame.size.height
            self.tblHistory.frame = tblMyRequest
            // self.scrollRequest.addSubview(self.tblMyRequest)
            
            var getupcomesTbl = self.viewSummary.frame
            getupcomesTbl.size.width = self.screenWidth
            getupcomesTbl.origin.x =  self.screenWidth
            getupcomesTbl.size.height = self.scrollRequest.frame.size.height
            self.viewSummary.frame = getupcomesTbl
            // self.scrollRequest.addSubview(self.tblMyTeam)
            
            var gettteamLeaveTbl = self.tblTeamleave.frame
            gettteamLeaveTbl.size.width = self.screenWidth
            gettteamLeaveTbl.origin.x =  self.screenWidth * 2
            gettteamLeaveTbl.size.height = self.scrollRequest.frame.size.height
            self.tblTeamleave.frame = gettteamLeaveTbl
            
            self.scrollRequest.contentSize = CGSize(width: self.screenWidth * 3, height: self.scrollRequest.frame.size.height)
            self.updateCornerradious(sender: self.managerSheet.first!)
            
        }
    }
    
    // MARK: - side menu
    @IBAction func openSideMenu(_ sender: UIBarButtonItem) {
        openSideMenu()
    }
    
    // MARK: - handle Request
    @IBAction func handleRequest(_ sender: UIButton) {
        updateCornerradious(sender: sender)
        
    }
    @objc func cellTapped() {
        openEditLeave(type: "history")
        
    }
    @objc func cellTeamTapped() {
        openEditLeave(type: "team")
        
    }
    func openEditLeave(type: String) {
        let applyLeave: EditLeaveViewController = UIStoryboard(storyboard: .main).instantiateViewController()
        applyLeave.getType =  type
        AppUtils.appUtilityPush(self, applyLeave)
    }
}

extension LeaveRequestViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView.tag == 3)
        {
            return 3
        }
        return tableView == tblHistory ? 4 : tableView ==  tblSummary ? 7 : 5
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblHistory{
            let cell = tableView.dequeueReusableCell(withIdentifier: historyIentifier, for: indexPath) as! LeaveHistoryCell
            let title = ["Leave Type\nSick", "Application Id\n123456", "End Date\n19 Nov 2018" ,"Start Date\n18 Nov 2018" ]
              cell.updateuserData(btn1Text: title)
            cell.btnViewRequest.forEach{$0.addTarget(self, action: #selector(cellTapped), for: .touchUpInside)}
            return cell
        }else  if tableView == tblSummary {
            let cell = tableView.dequeueReusableCell(withIdentifier: summaryIentifier, for: indexPath) as! SummaryCell
            var title = ""
            var midTitle = ""
            var subTitle = ""
            if indexPath.row == 0{
                title = "Annual"
                midTitle = "28"
                subTitle = "15"
                
            }else if indexPath.row == 1{
                title = "Sick"
                midTitle = "12"
                subTitle = "6"
            }else if indexPath.row == 2{
                title = "Emergency"
                midTitle = "5"
                subTitle = "2"
            }else if indexPath.row == 3{
                title = "unpaid"
                midTitle = "5"
                subTitle = "2"
            }else if indexPath.row == 4{
                title = "study"
                midTitle = "6"
                subTitle = "6"
            }else if indexPath.row == 5{
                title = "Maternity"
                midTitle = "10"
                subTitle = "10"
            }else if indexPath.row == 6{
                title = "Marriage"
                midTitle = "10"
                subTitle = "10"
            }
            cell.textLabel?.text = title
            cell.lblEntitled?.text = midTitle
             cell.detailTextLabel?.text = subTitle
            
            //  cell.updateTeamData()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: teamLeaveIentifier, for: indexPath) as! TeamLeavesCell
            let title = ["Leave Type\nSick", "Application Id\n123456", "Staff Id\n25", "Days\n3", "Start Date\n18 Nov 2018", "End Date\n20 Nov 2018"]
            cell.btnViewRequest.forEach{$0.addTarget(self, action: #selector(cellTeamTapped), for: .touchUpInside)}
            cell.updateuserData(btn1Text: title)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if   tableView == tblSummary {
            return
        }
        let applyLeave: EditLeaveViewController = UIStoryboard(storyboard: .main).instantiateViewController()
        applyLeave.getType =  tableView == tblHistory ? "history" : "team"
        AppUtils.appUtilityPush(self, applyLeave)
        
 
    }
    
}

extension LeaveRequestViewController:  UIScrollViewDelegate{
    
    // MARK: Scrollview delegates
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.x
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //  scrollDirectionDetermined = false
        if self.lastContentOffset < scrollView.contentOffset.x {
            // moved right
            movebuttonsShadows(scrollView: scrollView)
        } else if self.lastContentOffset > scrollView.contentOffset.x {
            // moved left
            movebuttonsShadows(scrollView: scrollView)
        }else{
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollOrientation = scrollView.contentOffset.y > lastPos.y ? .down : .up
        lastPos = scrollView.contentOffset
    }
    
    func movebuttonsShadows(scrollView: UIScrollView){
        let pageWidth: CGFloat = scrollView.frame.size.width
        let page: Int = Int(floor((scrollView.contentOffset.x - pageWidth / 3) / pageWidth) + 1)
        updateCornerradious(sender: page == 0 ? managerSheet.first! : page == 1 ? managerSheet[1] : managerSheet.last!)
        // print(page)
        
    }
    //MARK: - Class Methods
    func setContentOffset(position: CGFloat){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
                self.scrollRequest.contentOffset.x = position
            }, completion: nil)
        }
    }
    func updateCornerradious(sender: UIButton){
        for getBut in managerSheet{
            getBut.setTitleColor(.lightGray, for: .normal)
//            if let getTag = getBut.viewWithTag(101) {
//                getTag.removeFromSuperview()
//            }
        }
//        let lineView = UIView(frame: CGRect(x: 0, y: sender.frame.size.height, width: sender.frame.size.width, height: 2))
//        lineView.backgroundColor = UIColor.white
//        lineView.tag = 101
//        sender.addSubview(lineView)
        sender.setTitleColor(.white, for: .normal)
        btnAddLeave.isHidden = true
        
        if sender.tag == 1{
            self.scrollRequest.setContentOffset(.zero, animated: true)
            btnAddLeave.isHidden = false
        }else if  sender.tag == 2{
            self.scrollRequest.setContentOffset(CGPoint(x: self.screenWidth, y: 0), animated: true)
        }else  {
            self.scrollRequest.setContentOffset(CGPoint(x: self.screenWidth * 2, y: 0), animated: true)
        }
    }
}

