//
//  PayrollViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 28/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class PayrollCell: UITableViewCell{
    
    @IBOutlet weak var viewBGRequest: UIView!{
        didSet{
           viewBGRequest.layer.masksToBounds = false
           viewBGRequest.layer.cornerRadius = 3; // if you like rounded corners
            viewBGRequest.layer.shadowOffset = CGSize(width: -0.2, height: 0.2) //%%% this shadow will hang slightly down and to the right
           viewBGRequest.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
           viewBGRequest.layer.shadowOpacity = 0.2; //%%% same thing with this, subtle is better for me
            
            //%%% This is a little hard to explain, but basically, it lowers the performance required to build shadows.  If you don't use this, it will lag
           // let path = UIBezierPath(rect: viewBGRequest.bounds)
          //  viewBGRequest.layer.shadowPath = path.cgPath
//            UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.cardView.bounds];
//            viewBGRequest.layer.shadowPath = path.CGPath
           // viewBGRequest.enableCardView()
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var btnViewRequest: [UIButton]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnViewRequest.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true) }
    }
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
//        for index in 0...btn1Text.count - 1 {
//            btnViewRequest[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
//        }
    }
}

class PayrollViewController: MenuDisplayControllerViewController {
    
    @IBOutlet weak var tblPayroll: UITableView!{
        didSet {
            tblPayroll.clearEmptyCell()
            tblPayroll.rowHeight = 200.0
        }
    }
    
    let cellIentifier  = "cellPayroll"
    let userDetails = ["Period ID\n1234","From Date\n25 Nov 2018","To Date\n25 Nov 2018"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func rightBarButtonTapped(_ sender: Any) {
        let alert = CustomFilter(title: "Payroll")
        
        alert.completionBlock = {(dataReturned) -> ()in
            //Data is returned **Do anything with it **
            print(dataReturned)
        }
        alert.show(animated: true)
    }
    // MARK: - side menu
    @IBAction func openSideMenu(_ sender: UIBarButtonItem) {
        openSideMenu()
    }
    
    @objc func cellTapped() {
        let applyLeave: EditLeaveViewController = UIStoryboard(storyboard: .main).instantiateViewController()
        applyLeave.getType =   "payroll"
        AppUtils.appUtilityPush(self, applyLeave)
    }

}

extension PayrollViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIentifier, for: indexPath) as! PayrollCell
            cell.updateuserData(btn1Text: userDetails)
            cell.btnViewRequest.forEach{$0.addTarget(self, action: #selector(cellTapped), for: .touchUpInside)}
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let applyLeave: EditLeaveViewController = UIStoryboard(storyboard: .main).instantiateViewController()
        applyLeave.getType =   "payroll"
        AppUtils.appUtilityPush(self, applyLeave)
        
        
    }
    
}
