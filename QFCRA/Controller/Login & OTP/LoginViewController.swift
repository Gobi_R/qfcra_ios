//
//  LoginViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 20/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: UIViewController {
    
    @IBOutlet var txtArray: [UITextField]!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var labelTitle: UILabel! {
        didSet {
            labelTitle.enableMultilineText(txt: "Welcome to the \n  QFC Regulatory Authority", regularFontSize: 20, boldFontSize: 22)
        }
    }
    
    var activeTextField: UITextField!
    var viewWasMoved: Bool = false
    
    // MARK: - UI Loads
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateTextfieldUI()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.removeObservers()
    }
    
    func updateTextfieldUI(){
         hideKeyboardWhenTappedAround()
        for currentTxtField in txtArray{
            currentTxtField.withImage(direction: .Left, imageName: currentTxtField.tag == 1 ? "user_icon" : "password_icon", colorSeparator: UIColor.orange, colorBorder: UIColor.black)
        }
        btnLogin.enableCornerRadious(borderColor: .clear, status: false)
        observeKeyboard()
        
    }
    
    func observeKeyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo,  let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        if notification.name == UIResponder.keyboardWillHideNotification {
            // if self.viewWasMoved {h
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
         //   }
        } else  {
            // show the notification
           
         //   let activeTextFieldRect: CGRect? = activeTextField?.frame
          //  let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
          //  if (!self.view.frame.contains(activeTextFieldOrigin!)) {
          //      self.viewWasMoved = true
            if self.view.frame.origin.y == 0 {
             self.view.frame.origin.y -= activeTextField.frame.size.height 
            }
            
         //   } else {
                self.viewWasMoved = false
           // }
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= keyboardFrame.height
//            }
        }
         self.view.layoutIfNeeded()
    }
   
    // MARK: - Navigation
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showAndHideNavigationBar(status: true)
         // observeKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         showAndHideNavigationBar(status: false)
        txtArray.last?.text = ""
        txtArray.first?.text = ""
        removeObservers()
    }
    
    // MARK: - handle Login action
    @IBAction func LoginButtonPressed(_ sender: UIButton) {
      // connectionSegue(currentName: SegueName.showDashBoard.getsegName())
        
       connectionSegue(currentName: SegueName.openOTP.getsegName())
      /*
   // com.dci.Knekt
       guard let getusername = txtArray.first?.text, let getPass = txtArray.last?.text else {
        showAlert(title: "Username & Password fields are empty", message: "Please fill Username & Password", actionTitles: ["Okay"])
            return }
        if  getusername.isEmpty{
             showAlert(title: "Username should not be empty", message: "Please fill the Username field", actionTitles: ["Okay"])
      }else if getPass.isEmpty{
           showAlert(title: "Password should not be empty", message: "Please fill the Password field", actionTitles: ["Okay"])
        }else{
         // open OTP page
             connectionSegue(currentName: SegueName.openOTP.getsegName())
        }
         
        // direct open login
      // connectionSegue(currentName: SegueName.showDashBoard.getsegName())
        
//        self.view.endEditing(true)
//
//        // call webservice
//        connectionSegue(currentName: SegueName.openOTP.getsegName())
       // self.popupAlert(title: "Title", message: " Oops, xxxx ", actionTitles: ["Option1","Option2","Option3"], actions:[UIAlertAction.Style.destructive.rawValue ,UIAlertAction.Style.default.rawValue,UIAlertAction.Style.default.rawValue])
        */
    }
    
//    func popupAlert(title: String?, message: String?, actionTitles:[String?], actionStyles:[Int?], actions:[((UIAlertAction) -> Void)?]) {
//        // handle action
//    }
    
    func showAlert(title: String, message: String, actionTitles : [String]){
        self.popupAlert(title: title, message: message, actionTitles: actionTitles, actions:[{action1 in
            
            }, nil])
    }

}

extension LoginViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextResponder = textField.superview?.viewWithTag(textField.tag + 1) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
