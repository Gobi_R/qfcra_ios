//
//  OTPViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 21/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController, MyTextFieldDelegate {
    
     @IBOutlet var otpText: [MyTextField]!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var labelTitle: UILabel! {
        didSet {
            labelTitle.enableMultilineText(txt: "Welcome to the \n  QFC Regulatory Authority", regularFontSize: 20, boldFontSize: 22)
        }
    }
    
    var isReverse = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAndHideNavigationBar(status: false)
         hideKeyboardWhenTappedAround()
        //;
        otpText.forEach { $0.textColor  = .appBGColor; $0.myTextFieldDelegate = self; $0.keyboardType = .numberPad; $0.tintColor  = .appBGColor ; $0.addLineToView(view: $0, position:.LINE_POSITION_BOTTOM, color: .appBGColor, width: 0.8);  }
    // $0.setBottomBorder()
        otpText[0].becomeFirstResponder()
        btnSubmit.enableCornerRadious(borderColor: .clear, status: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         showAndHideNavigationBar(status: true)
    }
    
    // MARK: - handle OTP action
    @IBAction func btnResendOTPPage(_ sender: UIBarButtonItem) {
        
    }
    
    // MARK: - call OTPAPI
    func callOTPValidate(){
        var texts:  [String] = []
        otpText.forEach {  texts.append($0.text!)}
      //  sentOTPOption(currentText: texts.reduce("", +))
        
    }
    
     // MARK: - handle OTP action
    @IBAction func handleOTPSubmit(_ sender: UIButton) {
         connectionSegue(currentName: SegueName.oTPtoDashVC.getsegName())
    }
    
}

extension OTPViewController : UITextFieldDelegate {
    
    func textFieldDidEnterBackspace(_ textField: MyTextField) {
        guard let index = otpText.index(of: textField) else {
            return
        }
        if index > 0 {
            otpText[index - 1].becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
    }
    
//    }
//    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
//            return false
//        }
//
//        return true
//}
    func textFieldShouldReturnSingle(_ textField: UITextField, newString : String)
    {
        let nextTag: Int = textField.tag + 1
        textField.text = newString
        let nextResponder: UIResponder? = textField.superview?.viewWithTag(nextTag)
        if let nextR = nextResponder
        {
            // Found next responder, so set it.
            nextR.becomeFirstResponder()
        }
        else
        {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
            callOTPValidate()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
       
        if newString.count < 2 && !newString.isEmpty {
            textFieldShouldReturnSingle(textField, newString : newString)
            //  return false
        }
        return newString.count < 2 || string == ""
    }
//
//    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
//            return false
//        }
//        return true
    
}

class MyTextField: UITextField {
    weak var myTextFieldDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        if text?.isEmpty ?? false {
            myTextFieldDelegate?.textFieldDidEnterBackspace(self)
        }
        
        super.deleteBackward()
    }
}

protocol MyTextFieldDelegate: class {
    func textFieldDidEnterBackspace(_ textField: MyTextField)
}
