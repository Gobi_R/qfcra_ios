//
//  PRViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 22/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class MyRequest: UITableViewCell{
    
    @IBOutlet weak var viewBGRequest: UIView!{
        didSet{
             viewBGRequest.enableCardView()
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var btnViewRequest: [UIButton]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnViewRequest.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true) }
    }
//    func updateuserData(){
//       self.lblTitle.text = "name"
//        self.lblSubTitle.text = "Abdul"
//      self.btnViewRequest.first?.enableMultilineText(buttonText: "name\nAbdul", currentImage: "idcard_icon")
//        self.btnViewRequest.last?.enableMultilineText(buttonText: "name\nDCI", currentImage: "idcard_icon")
//    }
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
        //        self.lblTitle.text = title
        //        self.lblSubTitle.text = subTitle
        for index in 0...btn1Text.count - 1{
            btnViewRequest[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
        }
}
}

class MyTeamRequest: UITableViewCell{
    
    @IBOutlet weak var viewBGRequest: UIView!{
        didSet{
            viewBGRequest.enableCardView()
             
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var btnViewRequest: [UIButton]!
    @IBOutlet weak var lbluserName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnViewRequest.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true) }
    }
    
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
        //        self.lblTitle.text = title
        //        self.lblSubTitle.text = subTitle
        for index in 0...btn1Text.count - 1{
            btnViewRequest[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
        }
    }
}

class PRViewController: MenuDisplayControllerViewController {

    @IBOutlet weak var tblMyTeam: UITableView!{
        didSet {
            tblMyTeam.clearEmptyCell()
            tblMyTeam.rowHeight = 250.0
    }
    }
    
    @IBOutlet weak var tblMyRequest: UITableView!{
        didSet {
            tblMyRequest.clearEmptyCell()
            tblMyRequest.rowHeight = 200.0
           
        }
    }
    
    @IBAction func rightBarButtonTapped(_ sender: Any) {
        let alert = CustomFilter(title: "Purchase Request")
        alert.show(animated: true)
    }
    
    
    @IBOutlet weak var scrollRequest: UIScrollView!
    @IBOutlet var managerSheet: [UIButton]!
    
    let screenWidth = UIScreen.main.bounds.size.width
    let screenHeight = UIScreen.main.bounds.size.height
    
    let myRqIentifier  = "cellMyRequest"
    let myTeamRqIentifier  = "cellMyTeamRequest"
    
    var scrollOrientation: UIImage.Orientation?
    var lastContentOffset: CGFloat = 0.0
    var lastPos = CGPoint.zero
     var isMyteamPr = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tblMyRequest.register(MyRequest.self, forCellReuseIdentifier: myRqIentifier)
      //  self.tblMyTeam.register(MyTeamRequest.self, forCellReuseIdentifier: myTeamRqIentifier)

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            
            self.scrollRequest.translatesAutoresizingMaskIntoConstraints = false
            self.tblMyTeam.translatesAutoresizingMaskIntoConstraints = false
            self.tblMyRequest.translatesAutoresizingMaskIntoConstraints = false
            
            var getScroll = self.scrollRequest.frame
            getScroll.size.width = self.screenWidth
             self.scrollRequest.frame = getScroll
          
            var tblMyRequest = self.tblMyRequest.frame
            tblMyRequest.size.width = self.screenWidth
            tblMyRequest.origin.x = 0
            tblMyRequest.size.height = self.scrollRequest.frame.size.height
            self.tblMyRequest.frame = tblMyRequest
           // self.scrollRequest.addSubview(self.tblMyRequest)
           
            var getupcomesTbl = self.tblMyTeam.frame
            getupcomesTbl.size.width = self.screenWidth
            getupcomesTbl.origin.x =  self.screenWidth
            getupcomesTbl.size.height = self.scrollRequest.frame.size.height
            self.tblMyTeam.frame = getupcomesTbl
           // self.scrollRequest.addSubview(self.tblMyTeam)
            
            self.scrollRequest.contentSize = CGSize(width: self.screenWidth * 2, height: self.scrollRequest.frame.size.height)
             self.updateCornerradious(sender: self.managerSheet.first!)
        }
    }
    
    // MARK: - side menu
    @IBAction func openSideMenu(_ sender: UIBarButtonItem) {
          openSideMenu()
    }
    
    // MARK: - handle Request
    
    @IBAction func handleRequest(_ sender: UIButton) {
        updateCornerradious(sender: sender)
        sender.tag == 1 ? (isMyteamPr = false) : (isMyteamPr = true)
    }
    
    /*Cell Button Action*/
    @objc func myRequestCellTapped() {
        openEditLeave(type: 1)
    }
    
    @objc func myTeamCellTapped() {
        openEditLeave(type: 2)
        
    }
    func openEditLeave(type: Int) {
         connectionSegue(currentName: SegueName.prdetails.getsegName())
//        let PRDetail : PRDetailViewController = UIStoryboard(storyboard: .main).instantiateViewController()
//        PRDetail.getTypeofSender = type
//        AppUtils.appUtilityPush(self, PRDetail)
    }
    

}

extension PRViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == tblMyTeam ? 4 : 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblMyRequest{
            let cell = tableView.dequeueReusableCell(withIdentifier: myRqIentifier, for: indexPath) as! MyRequest
            cell.updateuserData(btn1Text: ["PR Name\nABCDEF","PR Number\n123456","Total Amount\n3000"])
            cell.btnViewRequest.forEach{$0.addTarget(self, action: #selector(myRequestCellTapped), for: .touchUpInside)}
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: myTeamRqIentifier, for: indexPath) as! MyTeamRequest
            cell.updateuserData(btn1Text: ["PR Name\nABCDEF","PR Number\n123456","Requester Id\n123456","Total Amt of PR\n3000",])
            cell.btnViewRequest.forEach{$0.addTarget(self, action: #selector(myTeamCellTapped), for: .touchUpInside)}
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isMyteamPr == true {
            UserDefaults.standard.set("team", forKey: "prteam")
        }
        connectionSegue(currentName: SegueName.prdetails.getsegName())
       // let PRDetail : PRDetailViewController = UIStoryboard(storyboard: .main).instantiateViewController()
      //  PRDetail.getTypeofSender = tableView == tblMyRequest ? 1 : 2
       // AppUtils.appUtilityPush(self, PRDetail)
    }
    
}

extension PRViewController:  UIScrollViewDelegate{
    
    // MARK: Scrollview delegates
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.x
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //  scrollDirectionDetermined = false
        if self.lastContentOffset < scrollView.contentOffset.x {
            // moved right
            movebuttonsShadows(scrollView: scrollView)
        } else if self.lastContentOffset > scrollView.contentOffset.x {
            // moved left
            movebuttonsShadows(scrollView: scrollView)
        }else{
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollOrientation = scrollView.contentOffset.y > lastPos.y ? .down : .up
        lastPos = scrollView.contentOffset
    }
    
    func movebuttonsShadows(scrollView: UIScrollView){
        let pageWidth: CGFloat = scrollView.frame.size.width
        let page: Int = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        updateCornerradious(sender: page == 0 ? managerSheet.first! : managerSheet.last!)
        // print(page)
        
    }
    //MARK: - Class Methods
    func setContentOffset(position: CGFloat){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.scrollRequest.contentOffset.x = position
            }, completion: nil)
        }
    }
    func updateCornerradious(sender: UIButton){
        for getBut in managerSheet{
            getBut.setTitleColor(.lightGray, for: .normal)
            if let getTag = getBut.viewWithTag(101) {
                getTag.removeFromSuperview()
            }
        }
        let lineView = UIView(frame: CGRect(x: 0, y: sender.frame.size.height, width: sender.frame.size.width, height: 2))
        lineView.backgroundColor = UIColor.white
        lineView.tag = 101
        sender.addSubview(lineView)
         sender.setTitleColor(.white, for: .normal)
     
       
        if sender.tag == 1{
            self.scrollRequest.setContentOffset(.zero, animated: true)
        }else if  sender.tag == 2{
            self.scrollRequest.setContentOffset(CGPoint(x: self.screenWidth, y: 0), animated: true)
        }
    }
}

