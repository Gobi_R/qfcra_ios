//
//  PRDetailsTableViewController.swift
//  QFCRA
//
//  Created by Gobi R. on 07/12/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class PRDetailsTableViewController: AccordionTableViewController {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var vwFooterView: UIView!
    @IBOutlet var lblRequester: [UILabel]!
    @IBOutlet var lblApprovalStatus: [UILabel]!
    @IBOutlet var lblMyApprovalStatus: [UILabel]!
    
    
    
    let screenSize = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblRequester.forEach{$0.isHidden = true}
        lblApprovalStatus.forEach{$0.isHidden = true}
        //let deviceheight = UIScreen.main.bounds.height / 2
        //self.view.conte = UIEdgeInsetsMake(0,0,0,50)
       // self.tableView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 50,right: 0)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let getValue = UserDefaults.standard.value(forKey: "prteam") as? String, !getValue.isEmpty, getValue == "team" {
            self.vwFooterView.translatesAutoresizingMaskIntoConstraints = true
            self.vwFooterView.frame =  CGRect(x: 0, y: self.screenSize.height - 44 - self.getsafeAreaBottomMargin(), width: self.screenSize.width, height: 44)
            self.vwFooterView.tag = 101
            UIApplication.shared.keyWindow?.addSubview(self.vwFooterView)
            UIApplication.shared.keyWindow?.bringSubviewToFront(self.vwFooterView)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.removeObject(forKey: "prteam")
        UIApplication.shared.keyWindow?.viewWithTag(101)?.removeFromSuperview()
    }
    
    func getsafeAreaBottomMargin() -> CGFloat {
        if #available(iOS 11.0, *) {
            let currentwindow = UIApplication.shared.windows.first
            return (currentwindow?.safeAreaLayoutGuide.owningView?.frame.size.height)! - (currentwindow?.safeAreaLayoutGuide.layoutFrame.size.height)! - (currentwindow?.safeAreaLayoutGuide.layoutFrame.origin.y)!
        }
        else {
            return 0
        }
    }
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "prdCell", for: indexPath) as! PRDetailsTableViewCell
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 240.0
    }
    
    @IBAction func btnApproveStatusAction(_ sender: UIButton) {
        let alert = CustomAlert(title: sender.currentTitle! )
        alert.show(animated: true)
        UserDefaults.standard.set(alert.dialogView.frame.origin.y, forKey: "dialogFrame")
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 300 : 30
    }
    
    func expandFirstCell() {
        let firstCellIndexPath = IndexPath(row: 3, section: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
}

class PRDetailsTableViewCell: AccordionTableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    @IBOutlet weak var lblValid: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var dropIcon: UIImageView! {
        didSet {
            dropIcon.tintColor = .appBGColor
        }
    }
    
    override func setExpanded(_ expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)
        
        if animated {
            UIView.transition(with: detailView, duration: 0.3, animations: {
                self.detailView.isHidden = !expanded
                self.toggleCell()
            }, completion: nil)
        } else {
            toggleCell()
            self.detailView.isHidden = !expanded
        }
    }
    private func toggleCell() {
        // detailView.isHidden = !expanded
        dropIcon.transform = expanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
    }
}
