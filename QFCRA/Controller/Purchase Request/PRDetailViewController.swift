//
//  PRDetailViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 23/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit

class PRDetailCell: UITableViewCell{
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    @IBOutlet weak var lblValid: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet var btnCollection: [UILabel]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
//        for index in 0...btn1Text.count - 1{
//            btnCollection[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
//        }
    }
}

class PRDetailViewController: UIViewController {

    @IBOutlet weak var viewBtnApprove: UIView!
    @IBOutlet weak var viewRequesterDetails: UIView!{
        didSet{
            viewRequesterDetails.enableCardView()
        }
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTypeofStatus: UILabel!
    @IBOutlet weak var lblmyRequestStatus: UILabel!
    @IBOutlet weak var lblmyRequestValue: UILabel!
    
    @IBOutlet weak var tbluserDetails: UITableView!{
        didSet{
         // tbluserDetails.estimatedRowHeight = 200.0
          tbluserDetails.rowHeight = 200.0  //UITableView.automaticDimension
          tbluserDetails.clearEmptyCell()
        }
    }
    @IBOutlet var btnUserDetails: [UIButton]!
    @IBOutlet var handleAction: [UIButton]!
    
    var getTypeofSender: Int = 0
    
    let cellIdentifier  = "prCell"
    let userDetails = ["PR Name\nABCDEF","Date\n12/11/2018","Requster ID\n123456","Total Amount\n3000"]
    let titlesForCell = ["Qty\n12", "Unit\n12", "Currency\nQATAR" ,"Unit Price\nQR250", "Net Amount\nQR300" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideMyRequestDetail()
        updateuserData(btn1Text: userDetails)
    }
    
    func hideMyRequestDetail(){
        btnUserDetails.forEach { $0.enableCornerRadious(borderColor: .appPRbuttonColor, status: true)  }
      
            DispatchQueue.main.async {
                if self.getTypeofSender == 1 {
                    self.btnUserDetails.last?.isHidden =  true
                    self.lblName.isHidden = true
                    self.lblTitle.isHidden = true
                    self.lblStatus.isHidden = true
                    self.lblTypeofStatus.isHidden = true
                    self.viewBtnApprove.isHidden = true
                    self.lblmyRequestStatus.isHidden = false
                    self.lblmyRequestValue.isHidden = false
//                self.tbluserDetails.translatesAutoresizingMaskIntoConstraints = false
//
//                var tblMyRequest = self.tbluserDetails.frame
//                tblMyRequest.size.height = self.viewBtnApprove.frame.origin.y +  self.viewBtnApprove.frame.size.height + tblMyRequest.origin.y +  tblMyRequest.size.height
//                self.tbluserDetails.frame = tblMyRequest
            
                } else {
                    self.lblmyRequestStatus.isHidden = true
                    self.lblmyRequestValue.isHidden = true
                }
        }
        
    }
    
    // MARK: - download
    @IBAction func btnDownload(_ sender: UIBarButtonItem) {
    }
    
    // MARK: - approve action
    @IBAction func handleApprove(_ sender: UIButton) {
        let alert = CustomAlert(title:  sender.tag == 1 ? "Approve" : "Reject" )
        alert.show(animated: true)
        UserDefaults.standard.set(alert.dialogView.frame.origin.y, forKey: "dialogFrame")
    }
    
    func updateuserData(btn1Text: [String]){ //title: String, subTitle: String, btnImagename: String,currentIndex: Int
        for index in 0...btn1Text.count - 1{
            btnUserDetails[index].enableMultilineText(buttonText: btn1Text[index], currentImage: "")
        }
    }
    /*Setting texviews height*/
    func setTxtViewHt(txtView: UITextView) {
        txtView.translatesAutoresizingMaskIntoConstraints = false
        txtView.sizeToFit()
        txtView.isScrollEnabled = false
    }
}

extension PRDetailViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PRDetailCell
        cell.updateuserData(btn1Text: titlesForCell)
     //   setTxtViewHt(txtView: cell.txtDescription)
        return cell
    }
    
    
}
