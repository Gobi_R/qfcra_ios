//
//  DashBoardViewController.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 21/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit


class ApprovalCell : UICollectionViewCell{
    
    @IBOutlet weak var imgBackground: UIView!{
        didSet{
            imgBackground.enableCardView()
        }
    }
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgBackground.enableCardView()
    }
    
}

class DashBoardViewController: MenuDisplayControllerViewController {
    
    @IBOutlet weak var btnApplyLeave: UIButton!
    @IBOutlet weak var btnClaim: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    
    @IBOutlet weak var dashBoardCollectionView: UICollectionView!
    
    let collectionCellIdentifier = "taskCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        enableCardLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showAndHideNavigationBar(status: false)
        
    }
    func enableCardLayout(){
        btnClaim.enableCardbuttons()
        btnApplyLeave.enableCardbuttons()
    }
    
    // MARK: - Navigation
    @IBAction func handleClaimAndLeave(_ sender: UIButton) {
        if sender.tag ==  1{ // apply leave
           
            let applyLeave: ApplyLeaveViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            AppUtils.appUtilityPush(self, applyLeave)
            
        }else { // claim new form
              let addClaim: AddClaimViewController = UIStoryboard(storyboard: .main).instantiateViewController()
               AppUtils.appUtilityPush(self, addClaim)
        }
    }
    
    // MARK: - back button action
    @IBAction func sideMenuBtnaction(_ sender: UIBarButtonItem) {
       openSideMenu()
    }
}

extension DashBoardViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  10
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: 130) //collectionViewSize/2
      
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//            if arrayCount % 2 == 0 {
//               // even number dont split the cell
//            }else{
//               // odd number move last cell
//        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellIdentifier, for: indexPath) as! ApprovalCell
         var frame: CGRect? = cell.frame
        // frame?.origin.x = 0
        var imageTitle = ""
        var title = ""
        var subTitle = ""
        if indexPath.item == 0{
            imageTitle = "expenseclaim_icon"
            title = "Expense Claim"
            subTitle = "12"
            
        }else if indexPath.item == 1{
            imageTitle = "leave_icon"
            title = "Leave"
            subTitle = "5"
            
        } else if indexPath.item == 2{
            imageTitle = "purchaserequistion_icon"
            title = "Purchase Requisition"
            subTitle = "7"
            
        } else if indexPath.item == 3{
            imageTitle = "budget_icon"
            title = "Budget"
            subTitle = "10"
            
        }else{
            imageTitle = "payroll_icon"
            title = "Payroll"
            subTitle = "10"
            frame?.origin.x  = cell.contentView.frame.size.width / 2
            cell.frame = frame ?? .zero
            
            
        }
        
        
        cell.imgCell.image = UIImage(named: imageTitle)
        cell.lblTitle.text = title
        cell.lblSubTitle.text = String(format:"Pending: %@",subTitle)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        //
        if indexPath.row == 0{
            let applyLeave: ClaimsViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            AppUtils.appUtilityPush(self, applyLeave)
        }else  if indexPath.row == 1{
            let applyLeave: LeaveRequestViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            AppUtils.appUtilityPush(self, applyLeave)

        }else  if indexPath.row == 2{
            let applyLeave: PRViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            AppUtils.appUtilityPush(self, applyLeave)
        }else  if indexPath.row == 3{
                        let applyLeave: BudgetAllocationViewController = UIStoryboard(storyboard: .main).instantiateViewController()
                        AppUtils.appUtilityPush(self, applyLeave)
        }else{
                        let applyLeave: PayrollViewController = UIStoryboard(storyboard: .main).instantiateViewController()
                        AppUtils.appUtilityPush(self, applyLeave)
        }
    }
}



