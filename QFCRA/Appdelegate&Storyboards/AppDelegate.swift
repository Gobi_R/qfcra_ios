//
//  AppDelegate.swift
//  QFCRA
//
//  Created by Karthikeyan A. on 20/11/18.
//  Copyright © 2018 Karthikeyan A. All rights reserved.
//

import UIKit
import HockeySDK


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // set the global Theme
        setGlobalTheme()
        return true
    }
    
    // MARK: - set Gloabl Theme
    func setGlobalTheme(){
        // navigation bar color
        UINavigationBar.appearance().barTintColor = .appBGColor
        UINavigationBar.appearance().backgroundColor = .appBGColor
        UINavigationBar.appearance().tintColor = .white
       // UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white,NSAttributedString.Key.font: UIFont(name: "Century Gothic", size: 17)!]
        UITextField.appearance().tintColor = .appBGColor
        // hockey app Installation
        BITHockeyManager.shared().configure(withIdentifier: "bd8235fc17634affb52219608bfe16f4")
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()

    }

}

